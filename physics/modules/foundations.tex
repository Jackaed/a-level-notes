\chapter{Foundations in Physics}

\section{Physical Quantities and Units}

\subsection{Physical Quantities}
A physical quantity has a numerical value and a unit.

\subsection{S.I. Units}
You do not need to know all of the S.I. units, only the following:
\begin{center}
	\begin{tabular}{|c|c|c|}
		\hline
		Quantity            & Unit           & Abbreviation \\
		\hline \hline
		Mass                & Kilogram       & \unit{\kilo \gram}           \\
		\hline
		Length              & Metre          & \unit{\meter}            \\
		\hline
		Time                & Seconds        & \unit{\second}            \\
		\hline
		Current             & Amperes (Amps) & \unit{\ampere}            \\
		\hline
		Temperature         & Kelvin         & \unit{\kelvin}            \\
		\hline
		Amount of Substance & moles          & \unit{\mole}          \\
		\hline
	\end{tabular}
\end{center}

These units are combined to form every other unit in the spec. For example, the Joule (J) is one
\(kg m^2 s^{-2}\). This can be derived from \(E_k = \frac{1}{2} m v^2\). Note that velocity also has
a composite unit here.

Each unit can also have a prefix, which multiplies the numerical value associated with the unit by
some value. This table represents the prefixes and what multiple they represent:

\begin{center}
	\begin{tabular}{|c|c|c|}
		\hline
		Prefix & Abbreviation & Multiplier            \\
		\hline \hline
		Femto  & \unit{\femto \ }      & \(\times 10 ^ {-15}\) \\
		\hline
		Pico   & \unit{\pico \ }     & \(\times 10 ^ {-12}\) \\
		\hline
		Nano   & \unit{\nano \ }       & \(\times 10 ^ {-9}\)  \\
		\hline
		Micro  & \unit{\micro \ }    & \(\times 10 ^ {-6}\)  \\
		\hline
		Milli  & \unit{\milli \ }        & \(\times 10 ^ {-3}\)  \\
		\hline
		Kilo   & \unit{\kilo \ }        & \(\times 10 ^ {3}\)   \\
		\hline
		Mega   & \unit{\mega \ }        & \(\times 10 ^ {6}\)   \\
		\hline
		Giga   & \unit{\giga \ }        & \(\times 10 ^ {9}\)   \\
		\hline
		Tera   & \unit{\tera \ }        & \(\times 10 ^ {12}\)  \\
		\hline
		Peta   & \unit{\peta \ }        & \(\times 10 ^ {15}\)  \\
		\hline
	\end{tabular}
\end{center}

Graph Axes are labelled as such: $<Quantity>/<Unit>$, with the independent variable on the \(x\) axis and
the dependent variable on the \(y\) axis.

\section{Making Measurements and Analyzing Data}

\subsection{Measurements and Uncertainties}

\subsubsection{Error}
There are two main types of error: systematic and random.

They have very different properties:

\begin{itemize}
	\item Random error gives each result a random offset, whereas systematic gives each result a
	      consistent offset or error.
	\item Random error can be dealt with by repeating measurements, whereas systematic error cannot.
	\item Some degree of random error is often unavoidable, whereas systematic error can be entirely eliminated.
\end{itemize}

\subsubsection{Precision and Accuracy}

There are two words that describe the quality of results: Precision and Accuracy:

\begin{itemize}
	\item A result is precise if it is similar to other results achieved by the same method.
	\item A result is accurate if it is close to the true value for that result.
\end{itemize}

These two things are distinct and should not be used interchangeably.

\subsubsection{Uncertainties}

Uncertainties can either be absolute or percentage.

When adding two data values with uncertainties, the resulting value has the uncertainty of the sum
of the two uncertainties of the data values. This also applies when subtracting.

When multiplying/dividing two data values with uncertainties, the resulting value has the uncertainty of the
sum of the percentage uncertainties of the two input values.

When raising a value to the power n, it's percentage uncertainty is multiplied by n.

\section{Nature of Quantities}

\subsection{Scalars and Vectors}

A vector quantity has magnitude, as well as direction, whereas a scalar value has only magnitude.

When adding vectors, you can either find the resulting vector using the triangle method. For
example, say we had vectors \(V_1 = (3, 2)\) and \(V_2 = (-1, 3)\), and wanted \(V_3 = V_1 + V_2\):

\begin{center}
	\begin{tikzpicture}
		\foreach \x in {0,1,2,3,4,5}
		\draw (\x cm,1pt) -- (\x cm,-1pt) node[anchor=north] {$\x$};
		\foreach \y in {0,1,2,3,4,5}
		\draw (1pt,\y cm) -- (-1pt,\y cm) node[anchor=east] {$\y$};
		\draw[step=1cm,gray,very thin] (0,0) grid (5, 5);
		\draw[thick,->] (0,0) -- (5,0);
		\draw[thick,->] (0,0) -- (0,5);
		\draw[thin,->] (0,0) -- (3, 2) node[anchor=north west] {\(V_1\)};
		\draw[thin, ->] (3, 2) -- (2, 5) node[anchor=north west] {\(V_2\)};
		\draw[thin, dashed, ->] (0, 0) -- (2, 5) node[anchor=south east] {\(V_3\)};
	\end{tikzpicture}
\end{center}

As you can see, \(V_3\) can be found by measuring the length of the triangle formed by placing
\(V_2\) on the end of \(V_1\).

This graph also shows that you can simply add the horizontal and vertical components of the vectors.
This can be done even if vectors are defined with angles, as \(V_x = |V| \cos(\theta)\) and \(V_y =
|V| \sin(\theta)\).