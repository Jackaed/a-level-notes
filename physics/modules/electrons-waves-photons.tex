\chapter{Electrons, Waves and Photons}

\section{Charge and Current}

\subsection{Charge}

\subsubsection{Quantisation of Charge}

The elementary charge \(e = 1.6 \times 10 ^ {-19}\) is the smallest possible subdivision of charge.

Charge is quantised, meaning that all charges are some multiple of \(e\).

\subsubsection{Current}

Current is defined as the rate of flow of charge. It can be written as:

\begin{equation}
	I = \frac{\Delta Q}{\Delta t}
\end{equation}

Where

\begin{itemize}
	\item[\(I\)] = Current / \unit{\ampere}
	\item[\(Q\)] = Charge / \unit{\coulomb}
	\item[\(t\)] = Time / \unit{\second}
\end{itemize}


Current refers to the movement of electrons in metals, or refers to the movement of ions in electrolytes.

Although electrons in a metal travel from the negative terminal of a power source to the positive
terminal, conventional current travels from positive to negative.

\subsubsection{Kirchoff's First Law}

Kirchoff's first law states that the sum of the currents entering a junction is equal to the sum of
the currents exiting a junction.

\subsection{Mean Drift Velocity}

The mean drift velocity in a material is the average velocity of the charged particles in the
material.

\begin{equation}
	I = nAev
\end{equation}

Where:

\begin{itemize}
	\item[\(I\)] = Current / \unit{\ampere}
	\item[\(n\)] = Number of charge carriers
	\item[\(A\)] = Cross sectional area of the conductor / \unit{\meter\squared}
	\item[\(e\)] = The elementary charge / \unit{\coulomb}
	\item[\(v\)] = Mean drift velocity / \unit{\meter\per\second}
\end{itemize}

\subsubsection{Insulators and Conductors}

In a conductor, there are a lot of freely available charge carriers able to carry a current.

In an insulator, there are few free charge carriers, meaning that it is harder for a current to be
carried.

In a semiconductor, there are more free charge carriers than in an insulator, but fewer
than in a conductor.

\section{Energy, Power and Resistance}

\subsection{Circuit Symbols}

\subsubsection{Battery}
\begin{center}
	\begin{tabular}{|c|c|}
		\hline
		Component Name                 & Circuit Symbol \\
		\hline \hline
		Battery                        &
		\begin{circuitikz}[european]
			\draw (0,0) to[battery] (2,0);
		\end{circuitikz}                   \\
		\hline
		Lamp                           &
		\begin{circuitikz}[european]
			\draw (0,0) to[lamp] (2,0);
		\end{circuitikz}                     \\
		\hline
		Switch                         &
		\begin{circuitikz}[european]
			\draw (0,0) to[nos] (2,0);
		\end{circuitikz}                     \\
		\hline
		Diode                          &
		\begin{circuitikz}[european]
			\draw (0,0) to[diode] (2,0);
		\end{circuitikz}                     \\
		\hline
		Potentiometer                  &
		\begin{circuitikz}[european]
			\draw (0,0) to[pR] (2, 0);
		\end{circuitikz}                     \\
		\hline
		Variable Resistor              &
		\begin{circuitikz}[european]
			\draw (0,0) to[vR] (2, 0);
		\end{circuitikz}                     \\
		\hline
		Ammeter                        &
		\begin{circuitikz}[]
			\draw (0,0) to[rmeter, t=A] (2, 0);
		\end{circuitikz}              \\
		\hline
		Voltmeter                      &
		\begin{circuitikz}[]
			\draw (0,0) to[rmeter, t=V] (2, 0);
		\end{circuitikz}              \\
		\hline
		Light Dependent Resistor (LDR) &
		\begin{circuitikz}[european resistors]
			\draw (0,0) to[ldR] (2, 0);
		\end{circuitikz}           \\
		\hline
		Capacitor                      &
		\begin{circuitikz}[european resistors]
			\draw (0,0) to[capacitor] (2, 0);
		\end{circuitikz}           \\
		\hline
		Thermistor                     &
		\begin{circuitikz}[european resistors]
			\draw (0,0) to[thermistor] (2, 0);
		\end{circuitikz}           \\
		\hline
		Light Emitting Diode (LED)     &
		\begin{circuitikz}[european resistors]
			\draw (0,0) to[led] (2, 0);
		\end{circuitikz}           \\
		\hline
	\end{tabular}
\end{center}

\subsection{E.M.F. and P.D.}

Potential difference (P.D.) is the energy transferred from electrical per unit charge.

Electromotive Force (E.M.F.) is the energy transferred to electrical per unit charge.

This means that:

\begin{equation}
	W = VQ = \mathcal{E} Q
\end{equation}
Where:
\begin{itemize}
	\item[\(W\)] = Work Done or Energy Transferred / \unit{\joule}
	\item[\(V\)] = Potential Difference / \unit{\volt}
	\item[\(Q\)] = Charge / \unit{\coulomb}
	\item[\(\mathcal{E}\)] = Electromotive Force / \unit{\volt}
\end{itemize}

\subsection{Resistance}

\begin{equation}\label{V=IR}
	V = IR
\end{equation}

Where:

\begin{itemize}
	\item[V] = Potential Difference / \unit{\volt}
	\item[I] = Current / \unit{\ampere}
	\item[R] = Resistance / \unit{\ohm}
\end{itemize}

Resistance is defined as the current through a resistor per unit potential difference.

\subsubsection{Ohm's Law}

Ohm's law says that the current through a conductor is directly proportional to the potential difference
across that conductor at constant temperature.

\subsubsection{I-V Characteristics}

I-V characteristics for:

\pgfplotsset{ticks=none}

\begin{enumerate}
	\item Resistor:
	      \begin{center}
		      \begin{tikzpicture}
			      \begin{axis}[
					      xmin=-2, xmax=2,
					      ymin=-2, ymax=2,
					      axis lines=center,
					      yticklabels={,,},
					      xticklabels={,,},
					      axis on top=true,
					      domain=-2:2,
					      ylabel=$I$,
					      xlabel=$V$,
				      ]
				      \addplot[mark=none, smooth] {\x};
			      \end{axis}
		      \end{tikzpicture}
	      \end{center}

	      This obeys ohm's law, as the gradient is constant.
	\item Filament Lamp
	      \begin{center}
		      \begin{tikzpicture}
			      \begin{axis}[
					      xmin=-2, xmax=2,
					      ymin=-1, ymax=1,
					      axis lines=center,
					      yticklabels={,,},
					      xticklabels={,,},
					      axis on top=true,
					      domain=-2.5:2.5,
					      ylabel=$I$,
					      xlabel=$V$,
				      ]
				      \addplot[mark=none, smooth] {tanh(\x)};
			      \end{axis}
		      \end{tikzpicture}
	      \end{center}
	      This does not obey ohm's law, as the temperature of a filament lamp changes depending on the P.D.
	      and current going through it.

	\item Diode and Light Emitting Diode (LED)

	      \begin{center}
		      \begin{tikzpicture}
			      \begin{axis}[
					      xmin=-5, xmax=5,
					      ymin=-0.5, ymax=1,
					      axis lines=center,
					      yticklabels={,,},
					      xticklabels={,,},
					      axis on top=true,
					      ylabel=$I$,
					      xlabel=$V$,
				      ]
				      \addplot[mark=none, smooth] {10^(\x - 3)};
			      \end{axis}
		      \end{tikzpicture}
	      \end{center}
	      This does not obey ohm's law as there is a critical P.D. which must be reached before the Diode
	      begins to conduct electricity.

	\item Thermistor
	      \begin{center}
		      \begin{tikzpicture}
			      \begin{axis}[
					      xmin=-1, xmax=1,
					      ymin=-1, ymax=1,
					      axis lines=center,
					      yticklabels={,,},
					      xticklabels={,,},
					      samples=35,
					      axis on top=true,
					      restrict y to domain=-10:10,
					      ylabel=$I$,
					      xlabel=$V$,
				      ]
				      \addplot[mark=none, smooth, tension=0.6] {x^3};
			      \end{axis}
		      \end{tikzpicture}
	      \end{center}
\end{enumerate}

This does not obey ohm's law, as when you increase the current and P.D. through the thermistor, it's
temperature increases, causing it's current to decrease.

\subsection{Resistivity}

The resistivity of a material is calculated using the following formula:

\begin{equation}\label{resistivity}
	R = \frac{\rho L}{A}
\end{equation}

Where:

\begin{itemize}
	\item[\(R\)] = Resistance / \unit{\ohm}
	\item[\(\rho\)] = Resistivity / \unit{\ohm \meter}
	\item[\(L\)] = Length of a wire / \unit{\meter}
	\item[\(A\)] = Cross sectional area of wire / \unit{\meter \squared}
\end{itemize}

The resistivity of a material is defined as the product of the unit resistance of that material and
the unit cross sectional area per unit length

\subsubsection{Determining resistivity}

Resistivity can be calculated using \ref{resistivity} by using vernier calipers to determine cross
sectional width, calculating the area, using a ruler to calculate the length and \ref{V=IR} to
graphically calculate the resistance with a voltmeter and ammeter.

\subsubsection{Temperature}

In most conducting metals, resistivity increases as temperature increases.

However, an NTC (Negative Temperature Coefficient) thermistor, the resistivity decreases as
temperature increases.

\subsection{Power}

\begin{equation}
	P = IV = I^2R = \frac{V^2}{R}
\end{equation}

Where:

\begin{itemize}
	\item[\(P\)] = Power / \unit{\watt}
	\item[\(I\)] = Current / \unit{\ampere}
	\item[\(V\)] = Potential Difference / \unit{\volt}
	\item[\(R\)] = Resistance / \unit{\ohm}
\end{itemize}

\begin{equation}
	W = VIt
\end{equation}

Where:

\begin{itemize}
	\item[\(W\)] = Work Done / \unit{\joule}
	\item[\(V\)] = Potential Difference / \unit{\volt}
	\item[\(I\)] = Current / \unit{\ampere}
	\item[\(t\)] = Time / \unit{\second}
\end{itemize}

\subsubsection{The Kilowatt Hour}

The Kilowatt Hour is a measure of energy. It is written as \unit{\kilo \watt \hour}. This
measurement is used when charging for electricity, as the price of energy is calculated in pounds
per kilowatt hour.

\section{Electrical Circuits}

\subsection{Series and Parallel Circuits}

\subsubsection{Kirchoff's Second law}

Kirchoff's Second Law states that in a closed loop, the sum of the E.M.F.s in the loop is equal to
the sum of the Potential Differences.

Kirchoff's Second Law conserves energy in a circuit.

\subsubsection{Resistors in Series and Parallel}

In a series circuit:

\begin{equation}
	R_T = R_1 + R_2 + \dots
\end{equation}

Where:

\begin{itemize}
	\item[\(R_T\)] = Total resistance / \unit{\ohm}
	\item[\(R_n\)] = Resistance of resistor \(n\) in the circuit / \unit{\ohm}
\end{itemize}

In a a parallel circuit:

\begin{equation}
	\frac{1}{R_T} = \frac{1}{R_1} + \frac{1}{R_2} + \dots
\end{equation}


\subsection{Internal Resistance}

In any power supply, there will be some amount of resistance stemming from the material used in the
power supply. This is called internal resistance.

This means that the P.D. across a cell will be lower than that cell's E.M.F. We call this drop in
P.D. the "lost volts", leading to the equations:

\begin{equation}
	\varepsilon = I(R + r)
\end{equation}

Where:

\begin{itemize}
	\item[\(\varepsilon\)] = E.M.F. of the power supply / \unit{\volt}
	\item[\(I\)] = Current in the Circuit / \unit{\ampere}
	\item[\(R\)] = Resistance of the Circuit / \unit{\ohm}
	\item[\(r\)] = Internal Resistance of the Cell / \unit{\ohm}
\end{itemize}

And:

\begin{equation}
	\varepsilon = V + Ir
\end{equation}

Where:

\begin{itemize}
	\item[\(\varepsilon\)] = E.M.F of the power supply / \unit{\volt}
	\item[\(V\)] = P.D. across the power supply / \unit{\volt}
	\item[\(I\)] = Current through the circuit / \unit{\ampere}
	\item[\(r\)] = Internal resistance of the power supply / \unit{\ohm}
\end{itemize}

\subsection{Potential Dividers}

A potential divider circuit is shown below:

\begin{center}
	\begin{circuitikz}[european resistors, scale=1.5]
		\draw(0,0) to[battery, l=\(V_{in}\)]  (0, 4) to[] (2, 4) to[resistor, l =\(R_1\)] (2, 2)
		to[resistor, l=\(R_2\)] (2, 0) to (0,0);
		\draw(2, 2) to (3, 2) node[ocirc]{};
		\draw(2, 0) to (3, 0) node[ocirc]{};
		\draw[->](3, 1.2) to (3, 1.9);
		\draw[->](3, 0.8) to (3, 0.1);
		\draw (3, 1) node[]{\(V_{out}\)};
	\end{circuitikz}
\end{center}

This circuit can be modelled with the equation:

\begin{equation}
	V_{out} = V_{in} \times \frac{R_2}{R_1 + R_2}
\end{equation}

\section{Waves}

\subsection{Wave Motion}

A progressive wave is a wave in the direction of energy transfer is perpendicular to the direction
of motion of the medium of the wave.

Transverse waves are waves in which the particles of the medium oscillate perpendicular to the
direction of wave propagation. In other words, the wave motion is transverse to the direction of
energy transfer. An example of a transverse wave is a wave on a rope or a string, where the
particles of the medium move up and down while the wave moves horizontally.

Longitudinal waves are waves in which the particles of the medium oscillate parallel to the
direction of wave propagation. In other words, the wave motion is longitudinal to the direction of
energy transfer. An example of a longitudinal wave is a sound wave in air, where the particles of
the medium compress and expand in the same direction as the wave travels.

\subsubsection{Properties of Waves}

Waves have the following properties:

\begin{itemize}
	\item Displacement - The distance from the equilibrium position in a particular direction
	\item Amplitude - The maximum displacement from the equilibrium position
	\item Wavelength - The minimum distance between two points oscillating in phase
	\item Period - The time taken for one complete wavelength to pass a given point
	\item Phase Difference - The difference between the displacements of particles along a wave, or the
	      difference between the displacements of particles on different waves
	\item Frequency The number of wavelengths passing a given point per unit time
	\item Speed - The distance travelled by a wave per unit time
\end{itemize}

\begin{equation}
	f = \frac{1}{T}
\end{equation}

Where:

\begin{itemize}
	\item[\(f\)] = Frequency / \unit{\ \per \second}
	\item[\(T\)] = Period / \unit{\second}
\end{itemize}

\begin{equation}
	v = f \lambda
\end{equation}

Where:

\begin{itemize}
	\item[\(v\)] = Velocity / \unit{\meter \per \second}
	\item[\(f\)] = Frequency / \unit{\ \per \second}
	\item[\(\lambda\)] = Wavelength / \unit{\meter}
\end{itemize}

\subsubsection{Reflection of Waves}

Reflection occurs when a wave changes direction at a boundary between two different media, remaining
in the original medium.

The law of reflection states that when a wave is reflected, the angle of incidence is equal to the
angle of reflection.

\subsubsection{Refraction}

Refraction occurs when a wave changes direction as it changes speed , occurring when the wave passes
from one medium to another.

Sound waves speed up when they enter a denser medium, whereas electromagnetic waves slow down.

When a wave slows down, it refracts towards the normal, and if it speeds up, it refracts away from
the normal.

Refraction does not effect the frequency of a wave, only it's wavelength.

\subsubsection{Diffraction}

Diffraction occurs when waves pass through a gap, which causes them to spread out.

\subsubsection{Polarisation}

When a wave is polarised, the particles oscillate along one direction only, meaning the wave is
confined to a single plane.

This wave is said to be plane polarised.

\subsubsection{Intensity}

The intensity of a progressive wave is defined as the radiant power passing through a surface per
unit area.

\begin{equation}
	I = \frac{P}{A}
\end{equation}

Where:

\begin{itemize}
	\item[\(I\)] = Intensity / \unit{\watt \per \meter \squared}
	\item[\(P\)] = Radiant Power / \unit{\watt}
	\item[\(A\)] = Area / \unit{\meter \squared}
\end{itemize}

This can be rearranged to:

\begin{equation}
	I = \frac{P}{4 \pi r^2}
\end{equation}

Where \(r\) is the distance from the source.

Intensity also relates to amplitude, with the following relationship: intensity \(\propto\)
amplitude\(^2\)

\subsection{Electromagnetic Waves}

Electromagnetic waves are waves which require no medium to travel.

They can be thought of as electric and magnetic fields oscillating at right angles to one another.

EM waves travel at the same speed through a vacuum. They can be reflected, refracted, diffracted,
and plane polarised.

This table shows the types of waves in the E.M. spectrum:

\begin{table}[htbp]
	\centering
	\begin{tabular}{@{}llllll@{}}
		\toprule
		Name         & Maximum Wavelength & Minimum Wavelength \\
		\midrule
		Radio        & \(10^{6}\)         & \(10^{-1}\)        \\
		Micro        & \(10^{-1}\)        & \(10^{-3}\)        \\
		Infra-red    & \(10^{-3}\)        & \(7^{-7}\)         \\
		Visible      & \(7^{-7}\)         & \(4^{-7}\)         \\
		Ultra-violet & \(4^{-7}\)         & \(10^{-8}\)        \\
		X-ray        & \(10^{-8}\)        & \(10^{-13}\)       \\
		Gamma        & \(10^{-10}\)       & \(10^{-16}\)       \\
		\bottomrule
	\end{tabular}
\end{table}

\subsubsection{Refractive Index}

The refractive index for a given material can be calculated using the following formula:

\begin{equation}
	n = \frac{c}{v}
\end{equation}

Where:

\begin{itemize}
	\item[\(n\)] = Refractive Index
	\item[\(c\)] = Speed of Light through a Vacuum / \unit{\meter \per \second}
	\item[\(v\)] = Speed of Light through the Material / \unit{\meter \per \second}
\end{itemize}

The refractive index can be used to calculate the angle at which a ray will refract, using:

\begin{equation}
	n_1 \sin \theta_1 = n_2 \sin \theta_2
\end{equation}

Note that theta is always the angle measured from the normal.

\subsubsection{Critical Angle}

Each material has a critical, which can be calculated using:

\begin{equation}
	\sin C = \frac{1}{n}
\end{equation}

This critical angle can be used to determine how light will reflect in a material.

\begin{itemize}
	\item \(\theta < C\) - Refraction and partial reflection occurs
	\item \(\theta = C\) - Light refracts along the boundary between the two media
	\item \(\theta > C\) - Total internal reflection occurs
\end{itemize}

\subsection{Superposition}

The principle of superposition states that when two waves meet at a point the resultant displacement
at that point is equal to the sum of the displacements of the individual waves

This is known as \textbf{interference}. When the interference increases the resultant amplitude of the wave,
this is known as constructive interference. When the interference decreases the resultant amplitude
of the wave, this is known as destructive interference.

\textbf{Coherence} refers to waves emitted from two sources with constant phase difference. To be
coherent then the waves must have the same frequency.

\textbf{Path difference} refers to the difference in distance travelled by two interfering waves.

\subsubsection{Young Double-Slit Experiment}

The young double-slit experiment demonstrated interference patters in E.M. waves, by shining
monochromatic light through two narrow slits next to one another, and observing the light that
appeared on a screen behind the two slits.

What you see is a number of dots appearing on the screen. This shows the constructive interference
where they dots appear, and the destructive interference where no light can be seen.

To calculate the wavelength of the light used in the experiment, you can use the following formula:

\begin{equation}
	\lambda = \frac{ax}{D}
\end{equation}

Where:

\begin{itemize}
	\item[\(\lambda\)] = Wavelength / \unit{\meter}
	\item[\(a\)] = Distance between the slits / \unit{\meter}
	\item[\(x\)] = Distance between the dots on the screen / \unit{\meter}
	\item[\(D\)] = Perpendicular distance from the screen to the slits / \unit{\meter}
\end{itemize}

\subsection{Stationary Waves}

A stationary wave forms when two waves with the same frequency travelling in opposite directions are
superposed.

At certain points, the two waves are in anti-phase, causing their displacements to cancel out,
forming a "node", where the displacement is always zero.

At other points, the waves are in phase, providing the point of greatest amplitude.

The separation between each node and anti-node is equal to half the wavelength of the original
progressive wave.

The frequency is the same as that of the original waves.

\subsubsection{Stationary Waves on Strings}

If a string is stretched between two fixed points, these points act as nodes. When the string is
plucked, a stationary wave is formed.

The wavelength of the wave is double the length of the string, and it vibrates at it's natural
frequency \(f_0\).

\subsubsection{Stationary Waves in Tubes}

A stationary wave produced in a tube must have an anti-node at any open end and must have a node at
any closed end.

\section{Quantum Physics}

\subsection{Photons}

The photon model of electromagnetic radiation models electromagnetic radiation as a series of
particles, called photons, travelling through space.

Whilst electromagnetic radiation is typically modelled as waves, this fails to explain details such
as the photoelectric effect, which is why the photon model exists.

A photon is a massless "quantum" of electromagnetic energy. This means that energy transfer is
discretised, rather than being continuous.

\subsubsection{Photon Energy}

\begin{equation}
	E = hf = \frac{hc}{\lambda}
\end{equation}

Where:

\begin{itemize}
	\item[\(E\)] = Photon Energy / \unit{\joule}
	\item[\(h\)] = Planck's Constant / \unit{\joule \second}
	\item[\(f\)] = Frequency of the EM radiation/ \unit{\joule} 
\end{itemize}

The electron volt is used as a unit of energy - 1 electron volt is the energy stored by a charge
with charge \(e\) and 1 volt of P.D.

1\unit{\electronvolt} = \(1.6 \times 10^{-19}\) \unit{\joule}.

\subsection{The Photoelectric Effect}

The photoelectric effect occurs when a photon hits an electron in an atom. If a photon has
sufficient energy, it displaces an electron from the atom, which is emitted. This causes the
material to become positively charged.

The emitted electrons are referred to as "photoelectrons".

Since each photon needs to have a certain amount of energy to displace an electron from the atom,
this causes a "threshold frequency" above which the photoelectric effect will occur, but below which
it will not.

The minimum amount of energy required to displace electrons from a particular material is known as
the "work function" of that material.

This gives rise to the following equation:

\begin{equation}
	hf = \phi + E_{k max}
\end{equation}

Where:

\begin{itemize}
	\item[\(f\)] = Frequency of the EM radiation / \unit{\hertz}
	\item[\(\phi\)] = Work Function of the material / \unit{\joule}
	\item[\(E_{k max}\)] = Maximum kinetic energy of released electrons / \unit{\joule}
\end{itemize}

\subsubsection{Intensity}

The intensity of the incident radiation is not relevant to the maximum kinetic energy of released
electrons, nor is it relevant to whether the photoelectric effect occurs at all. Instead, it simply
dictates the amount of electrons that are emitted.

The rate of emission of photoelectrons is directly proportional to the intensity of the incident
electromagnetic radiation.

\subsection{Wave-Particle Duality}

Electrons can also behave like waves, as discovered by Louis De Broglie. When electrons are fired at
a thin graphite film, they are diffracted, which can be seen on a phosphor screen as a number of
concentric rings. 

The de-broglie wavelength of a particle is simply the wavelength associated with a moving particle.

When particles are in motion, they behave as waves, but when they collide with things, they behave
as particles.

\begin{equation}
	\lambda = \frac{h}{p}
\end{equation}

Where:

\begin{itemize}
	\item[\(\lambda\)] = The De Broglie Wavelength / \unit{\meter}
	\item[\(p\)] = The Momentum of a Particle / \unit{\kilogram \meter \per \second} 
\end{itemize}
