\chapter{Forces and Motion}

\section{Motion}

\subsection{Kinematics}

You need to know that the area under a velocity-time graph represents displacement, and that the
gradient of the velocity-time graph is acceleration.

\subsection{Linear Motion}

\subsubsection{SUVAT Equations}

\begin{equation}v = u + at\end{equation}
\begin{equation}s = ut + \frac{1}{2}at^2\end{equation}
\begin{equation}s = \frac{1}{2}(u+v)t\end{equation}
\begin{equation}v^2=u^2+2as\end{equation}

Where:

\begin{itemize}
	\item[\(s\)] = Displacement / \unit{\meter}
	\item[\(u\)] = Initial Velocity / \unit{\meter \per \second}
	\item[\(v\)] = Final Velocity / \unit{\meter \per \second}
	\item[\(a\)] = Acceleration / \unit{\meter \per \second \squared}
	\item[\(t\)] = Time / \unit{\second}    
\end{itemize}

Note these equations only apply when acceleration is constant.

\subsubsection{Acceleration of Free Fall}

\(g = 9.81\) is the acceleration of an object in free fall due to gravity (on
average on Earth's surface).

\(g\) can be determined in a laboratory, where light gates are used to
calculate the time taken for an object to travel fall between two points, using SUVAT to calculate acceleration.

\subsubsection{Reaction Time and Thinking Distance}

Reaction time is defined as the time taken from a person needing to brake and that person starting
to brake.

Thinking distance is defined as the distance traveled by the vehicle from when the driver sees a
problem and the brakes are applied.

Braking distance is the distance traveled by a vehicle from when the brakes are applied to when the
vehicle stops. Braking distance increases with the square of velocity.

Reaction time, and subsequently thinking distance, can be impacted by a number of things, such as
distractions, drugs, etc.

Stopping distance is the sum of thinking distance and braking distance.

\subsection{Projectile Motion}

You must know that vertical and horizontal motion of a projectile are independent.

You must be able to use SUVAT equations on a projectile having split it's velocity into horizontal
and vertical components, using g as the acceleration in the vertical direction, and 0 as the
acceleration horizontally.

\section{Forces in Action}

\subsection{Dynamics}

\begin{equation}\label{f=ma}
	F = ma
\end{equation}

Where:
\begin{itemize}
	\item[\(F\)] = Force / \unit{\newton}
	\item[\(m\)] = Mass / \unit{\kilogram}
	\item[\(a\)] = Acceleration / \unit{\meter \per \second \squared}  
\end{itemize}

From \ref{f=ma} we can determine that:

\begin{equation}
	W = mg
\end{equation}

Where:
\begin{itemize}
	\item[\(W\)] = Weight / \unit{\newton}
	\item[\(m\)] = Mass / \unit{\kilogram}
\item[\(g\)] = Acceleration due to gravity / \unit{\meter \per \second \squared} = Gravitational
Field Strength / \unit {\newton \per \kilogram}  
\end{itemize}

\subsubsection{Common Types of Force}

Tension is defined as the force exerted by a body due to it's extension.

Normal Contact Force is defined as the force acting upon a body due to it being in contact with
another body.

Upthrust is defined as the force acting on a body due to the difference in pressure acting on the
body's top surface and it's bottom surface.

Friction is defined as the force resisting two surfaces sliding upon one another.

\subsection{Non-Uniform Acceleration}

Drag is defined as the frictional force experienced by an object travelling through a fluid.

Drag is affected by the frontal area of the object, as well as the object's coefficient of friction.

Terminal velocity is defined as the speed at which an object experiences a drag force equal to the
force of gravity acting upon the object.

Theoretically, objects in free fall approach but never quite reach their terminal velocity.

Terminal velocity in a fluid can be determined by repeatedly measuring the distance traveled in set
time intervals, then calculating the velocity once those distances become consistent.

\subsection{Equilibrium}

The moment of a force about a point is defined as the magnitude of the force multiplied with the perpendicular
distance to that point.

If there are two equal forces being applied in opposite directions about at equal distance to
particular point, those two forces form a couple, and they produce a torque, equal to the magnitude
of the forces multiplied by the perpendicular distance between the two forces.

For an object to be in Equilibrium, the net force acting on it must be zero, and the sum of the
anticlockwise moments must be equal to the sum of the clockwise moments on that object.

The centre of mass of an object is the point about which the mass of that object can be modelled as
being entirely centralized.

The centre of gravity of an object is the point at which the weight of that object can be modelled
as being entirely centralized.

In a uniform gravitational field, the centre of mass is the same as the centre of gravity.

If three forces are in Equilibrium, you will be able to form a triangle with those forces.

\subsection{Density and Pressure}

\begin{equation}\rho = \frac{m}{V}\end{equation} Where \(\rho\) is density, \(m\) is mass, and \(V\) is volume.

\begin{equation}p = \frac{F}{A}\end{equation} Where \(p\) is pressure, \(F\) is force, and \(A\) is area.

\begin{equation}p = h \rho g\end{equation} Where \(p\) is pressure, \(h\) is depth, \(\rho\) is density and \(g=9.81\)

Archimedes principle states that the upthrust acting on an object is equal to the weight of the
fluid displaced by that object.

\section{Work, Energy and Power}

\subsection{Work and Conservation of Energy}

\begin{equation}
	W = Fx \cos(\theta)
\end{equation}
Where \(W\) is work done, \(F\) is force applied, and \(x \cos(\theta)\) is
the displacement parallel to the direction of the force.

The principle of conservation of energy states that energy cannot be destroyed or created, only
transferred from form to form.

Work done is energy transferred.

\subsection{Kinetic and Potential Energies}

\begin{equation}
	E_k = \frac{1}{2}mv^2
\end{equation}

Where \(E_k\) is kinetic energy, \(m\) is the mass of an object, and \(v\) is it's velocity.

\begin{equation}
	E_p = mgh
\end{equation}
Where \(E_p\) is gravitational potential energy, \(m\) is the mass of an object, \(g\)
is the gravitational constant (9.81) and h is the height of the object.

When an object falls, it's gravitational potential energy is transferred to kinetic energy.

\subsection{Power}

\begin{equation}
	P = \frac{W}{t}
\end{equation}
Where \(P\) is power, \(W\) is work done, and \(t\) is the time taken.

\begin{equation}
	P = Fv
\end{equation}
Where \(P\) is power, \(F\) is the force applied, and \(v\) is the velocity of the object.

\begin{equation}
	{efficiency} = \frac{useful~output~energy}{total~input~energy}
\end{equation}

Note this gives a value between 0 and 1, and must be converted to a percentage.

\section{Materials}

\subsection{Springs}

Hooke's law states that the force applied to a spring is proportional to it's extension, unless the
elastic limit has been exceeded.

\begin{equation}
	F = kx
\end{equation}
Where \(F\) is the force applied to a spring, \(k\) is the spring constant for that spring, and
\(x\) is the spring's extension.

You can plot a graph of force on the x, and extension on the y, and the gradient will be
\(\frac{1}{k}\).

\subsection{Mechanical Properties of Matter}

In a force-extension graph, the area under the line of best fit will be the work done.

\begin{equation}
	E = \frac{1}{2}kx^2
\end{equation} 
Where \(E\) is elastic potential energy, \(k\) is the spring constant, and \(x\) is the
extension. This can also be written as \begin{equation}E = \frac{1}{2}Fx\end{equation}

\subsubsection{Stress, Strain and Ultimate Tensile Strength}

\begin{equation}
	\sigma = \frac{F}{A} \quad (N m ^ {-2}) 
\end{equation} 
Where \(\sigma\) is tensile stress, \(F\) is tensile force, and \(A\) is
cross sectional area.

\begin{equation}
	\varepsilon = \frac{x}{L}
\end{equation} 
Where \(\varepsilon\) is tensile strain, x is extension, and L is original
length. Tensile strain has no units.

The ultimate tensile strength of a material is the maximum breaking stress that can be applied to a
material.

The limit of proportionality for a material is the point up until which Hooke's law is obeyed.

The elastic limit for a material is the point at which it begins to experience plastic deformation.

\begin{equation}
	E = \frac {\sigma} {\varepsilon}
\end{equation} 
Where \(E\) is the Young modulus of a material, \(\sigma\) is the
tensile stress on that material, and \(\varepsilon\) is the tensile strain on that material.

You can calculate the young modulus of a wire by clamping it to a table, hanging it over a pulley,
and slowly attaching more and more weights to it, measuring its change in length with a ruler, and
cross sectional area with a micrometer. You then plot a graph of stress against strain, and get
Young Modulus from the straight line section of the graph.

\subsubsection{Elastic and Plastic deformations}

Elastic deformation is deformation where the material returns to it's original shape once the force
applied is removed.

Comparatively, plastic deformation is deformation where the material does not return to it's
original shape once the applied force is removed.

\subsubsection{Typical Stress-Strain Graphs}

Here is the stress-strain graph for a brittle material:

\begin{center}
	\begin{tikzpicture}[scale=3]
		% Axes
		\draw[thick,->, -latex] (0,0) -- (1.2,0) node[below] {Strain};
		\draw[thick,->, -latex] (0,0) -- (0,1.2) node[left] {Stress};

		% Stress-strain curve
		\draw[thick, ->, -{Latex[length=3mm, width=3mm]}] (0.399,0.399) -- (0.4,0.4);
		\draw[thick, ->, -{Latex[length=3mm, width=3mm]}] (0.601, 0.601) -- (0.6,0.6);
		\draw[thick] (0.0, 0.0) -- (1,1);


		% Labels
		\node[left] at (0,-0.1) {0};

	\end{tikzpicture}
\end{center}

For a brittle material, the ultimate tensile strength is equal to the breaking strength.

Here is the stress-strain graph for rubber:

\begin{center}
	\begin{tikzpicture}[scale=1.5]
		\begin{scope}[thick, decoration={
						markings,
						mark=at position 0.5 with {\arrow{Latex[length=3mm, width=3mm]}}}
			]
			% Stress-strain curve
			\draw[postaction={decorate}] plot [smooth, tension=1] coordinates{(0,0) (0.75, 1) (2.25, 1.5) (3, 2.5)};
			\draw[postaction={decorate}] plot [smooth, tension=1] coordinates{(3, 2.5) (2.25, 1.15) (0.75, 0.65) (0,0)};
		\end{scope}
		% Axes
		\draw[thick,->, -latex] (0,0) -- (3,0) node[below] {Strain};
		\draw[thick,->, -latex] (0,0) -- (0,2.5) node[left] {Stress};

		% Labels
		\node[left] at (0,-0.1) {0};
	\end{tikzpicture}
\end{center}

Here is the stress-strain graph for polythene:

\begin{center}
	\begin{tikzpicture}[scale=1.5]
		% Axes
		\draw[thick,->, -latex] (0,0) -- (3,0) node[below] {Strain};
		\draw[thick,->, -latex] (0,0) -- (0,2) node[left] {Stress};

		% Stress-strain curve
		\draw[thick] plot [smooth, tension=0.25] coordinates {(0,0) (0.5, 1.0) (3, 1.2)};

		% Labels
		\node[left] at (0,-0.1) {0};
	\end{tikzpicture}
\end{center}

Both polymeric materials stretch quite dramatically, but rubber shows plastic deformation, whereas
polythene shows plastic deformation.

Finally, here is the stress-strain graph for ductile materials, such as a metal wire:

\begin{center}
	\begin{tikzpicture}[scale=1.5]
		% Axes
		\draw[thick,->, -latex] (0,0) -- (3,0) node[below] {Strain};
		\draw[thick,->, -latex] (0,0) -- (0,2) node[left] {Stress};

		% Stress-strain curve
		\draw[thick] plot [smooth, tension=0.5] coordinates {(0,0) (0.5, 1.25) (0.75, 1.4) (1.0, 1.25)};
		\draw[thick] plot [smooth, tension=0.75] coordinates {(1.0, 1.25) (2.25, 2.0) (3.0, 1.75)};

		% Labels
		\node[left] at (0,-0.1) {0};
	\end{tikzpicture}
\end{center}

\section{Newton's Laws of Motion and Momentum}

\subsection{Newton's Laws of Motion}

Newtons three laws are:

\subsubsection{First}

A body that is at rest will stay at rest and a body that is in motion's velocity will remain
constant unless a resultant force is applied.

\subsubsection{Second}

The rate of change of momentum of an object is directly proportional to the resultant force and
takes place in the direction of the force.

\subsubsection{Third}

When two objects interact, each exerts an equal but opposite force on the other during the
interaction.

\begin{equation}
	p = mv
\end{equation} 
Where \(p\) is linear momentum, \(m\) is mass, and \(v\) is velocity in the direction of
that momentum.

Momentum is a vector.

\begin{equation}
	F = \frac{\Delta p}{\Delta t}
\end{equation} Where F is net force, and \(\frac{\Delta p}{\Delta t}\) is rate of
change of momentum.

\begin{equation}
	I = F \Delta t
\end{equation} 
Where I is the impulse of the force, F is the magnitude of the force and \(\Delta
t\) is the change in time.

Impulse is equal to the area under a force-time graph.

\subsection{Collisions}

In a collision, the total momentum before the collision is equal to the total momentum after the
collision in all directions.

Two deal with collisions in two dimensions, the momentum before and momentum after should be split
into it's respective x and y components, and resolved separately.

A perfectly elastic collision is one where the total kinetic energy before the collision is equal to
the total kinetic energy after the collision.

An inelastic collision is one where this is not true, and some of the energy has been transferred
from kinetic energy into other forms, such as heat.
