\chapter{Newtonian World and Astrophysics}

\section{Thermal Physics}

\subsection{Temperature}

Thermal Equilibrium is a state in which there is no net flow of thermal energy between the objects
involved.

Objects in thermal Equilibrium are at the same temperature.

The absolute scale of temperature (which measures temperature in Kelvin) is not dependant on a
property of a particular substance, instead it is directly proportional to the thermal energy in a
substance.

In Celsius, 0 degrees is the freezing point of water, and 100 degrees is the boiling point (at a
particular atmospheric pressure).

To convert from Celsius to Kelvin, simply add 273.

As a general rule, for all calculations involving temperature, convert to Kelvin.

\subsection{Solid, liquid and gas}

In solids, molecules are regularly arranged and packed closely together, with strong electrostatic
forces of attraction between them.

In liquids, the molecules are very close together, but have more kinetic energy, and can change
position and flow past one another.

In gases, molecules have even more kinetic energy than in liquids, and are far more spread out. The
electrostatic forces of attraction are negligible between them, and the molecules move randomly with
different speeds in different directions.

\subsubsection{Brownian Motion}

The random motion of particles in a gas is referred to as Brownian motion.

Brownian motion can be observed using a smoke cell and a microscope, where one can observe specks of
light if they look at the smoke through a microscope. This is because you can see the light being
scattered by the smoke particles, meaning that you are effectively observing the individual smoke
particles.

This proved brownian motion.

\subsubsection{Internal Energy}

The internal energy of a substance is defined as the sum of the random distribution of kinetic and
potential energies of the molecules of a substance.

As a substance's temperature increases, it's internal energy increases, as the kinetic energy of its
molecules increases.

When a substance changes state, the potential energies of the substance's molecules also change,
meaning that the substance's internal energy changes. For example, when going from a Liquid to a
Gas, the potential energy of the substance's molecules increase, so even if temperature does not
increase, the internal energy of the substance still increases.

When a substance is changing phase, it's temperature remains more or less constant, however it's
internal energy will continue to increase.

\subsubsection{Absolute Zero}

Absolute zero (0 K) is the lowest possible temperature, where each molecule has no kinetic energy.

\subsection{Thermal Properties of Materials}

\begin{equation}
	E = mc \Delta \theta
\end{equation}
Where \(E\) is energy transferred to that substance, m is the mass of that
substance, \(c\) is the specific heat capacity of the substance and \(\Delta \theta\) is the change in
temperature of that substance.

You must be able to explain how an experiment would be carried out to determine the specific heat
capacity of a metal or liquid, using an electrical heater, by calculating the energy transferred to
said electrical heater by measuring the current and P.D. transferred to the heater.

\subsubsection{Specific Latent Heat}

The specific latent heat of a substance is defined as the energy required to change the phase per
unit mass while at constant temperature.

\begin{equation}
	L = \frac{E}{m}
\end{equation}
Where \(L\) is the specific latent heat of the substance, \(E\) is the energy supplied
to change the phase of the mass \(m\) of the substance.

Substances have two latent heat values: Latent heat of fusion (the energy required to change a
kilogram of a substance's phase from solid to liquid) and the Latent heat of vaporisation (the
energy required to change a kilogram of a substance's phase from liquid to gas).

\subsection{Ideal Gases}

The Avogadro constant \(N_A\) represents the number of molecules in a mole of a substance, and is
equal to \(6.02 \times 10 ^ {23}\)

\subsubsection{Kinetic Theory of Gases}

The kinetic theory of gasses refers to the model used to describe the behaviour of the molecules in
an ideal gas.

A number of assumptions are made in the kinetic model of an ideal gas:

\begin{itemize}
	\item The gas contains a large number of atoms moving in random directions with random speeds.
	\item The atoms or molecules of the gas occupy a negligible volume compared to the volume of the gas.
	\item The collisions between molecules are perfectly elastic, as well as the collisions between
	      molecules and the container
	\item The time of collisions between the atoms or molecules is negligible
	\item Electrostatic forces between molecules are negligible except during  collisions
\end{itemize}

The molecules in an ideal gas cause pressure by colliding with the walls of the container. When they
do, their speed does not change, but velocity goes from \(u\) to \(-u\), so the total change in
momentum is \(-2mu\).

From Newton's second law, the force exerted on the molecule is equal to \(\frac{-2mu}{\Delta t}\).
From Newton's third law, the molecule exerts an equal but opposite force on the wall.

When a large number of molecules collide with the wall, they exert a total force \(F\) on the wall.
If \(A\) is the cross-sectional area of the wall, then \(p = \frac{F}{A}\).

\begin{equation}
	pV = nRT
\end{equation}
Where \(p\) is pressure, \(V\) is volume, \(n\) is the number of moles of the
substance, \(R\) is the molar gas constant, and T is the temperature of the substance.

This can be rearranged to
\begin{equation}
	\frac{P_1 V_1}{T_1} = \frac{P_2 V_2}{T_2}
\end{equation}
for a given number of moles
of a substance, allowing you to calculate how any one of pressure, volume or temperature will vary
given a variation in another variables.

\begin{equation}
	pV = \frac{1}{3}N m \overline{c ^ 2}
\end{equation}
Where is \(p\) is pressure of the gas, \(V\) is the volume
of gas, \(N\) is the
number of particles in the gas, \(m\) is the mass of the gas, and \(\overline{c ^ 2}\) is the mean square speed of the
particles in the gas.

\subsubsection{Root Mean Square Speed}

To calculate the r.m.s. speed of a gas, the velocity \(c\) of each particle in the gas is squared,
to find \(c^2\). This is then averaged across all particles, giving \(\overline{c^2}\). To find the
r.m.s of the gas, square root this, giving \(\sqrt{\overline{c^2}}\).

\subsubsection{Boltzmann Constant}

The Boltzmann Constant \(k = \frac{R}{N_A}\)

\begin{equation}
	pV = NkT
\end{equation}
Where \(p\) is pressure, \(V\) is volume, \(N\) is the number of particles, \(k\) is
the Boltzmann constant, and \(T\) is temperature.

\begin{equation}
	\frac{1}{2}m \overline{c^2} = \frac{3}{2} k T
\end{equation}
Where \(m\) is mass, \(\overline{c^2}\) is the
mean squared speed, \(k\) is the Boltzmann constant, and \(T\) is temperature. This can be
rearranged from \(pV = NkT\) and \(pV = \frac{1}{3}N m \overline{c ^ 2}\)

\subsubsection{Internal Energy}

Since for an ideal gas we assumed that the electrostatic forces between particles in the gas are
negligible, that means there is no potential energy in the gas, so the internal energy of an ideal
gas is equal to the kinetic energy of the particles.

This means if you double the temperature of an ideal gas, you also double it's internal energy.

\section{Circular Motion}

\subsection{Kinematics of Circular Motion}

The radian is alternative measurement of angle to the degree.

\begin{equation}
	2\pi \ \unit{\radian} = 360 \unit{\degree}
\end{equation}

The period of a circular motion is the time taken to complete a full rotation.

The frequency of circular motion is the number of complete rotations made by an object in circular
motion per unit time.

\begin{equation}
	\omega = \frac{2\pi}{T}
\end{equation}
Where \(\omega\) is the angular velocity of an object and \(T\) is the
period of the rotation.

This can be rearranged to:

\begin{equation}
	\omega = 2 \pi f\
\end{equation}

Angular velocity is defined as the rate of change of angle of an object. It is usually measured in
\(rad \ s^{-1}\).

\subsection{Centripetal Force}

Centripetal force is defined as a constant net force perpendicular to the velocity of an object
which causes it to travel in a circular path.

Centripetal acceleration is defined as the acceleration caused by a centripetal force.

\begin{equation}
	v = r \omega
\end{equation}
Where \(v\) is velocity, \(r\) is the radius of the circular path, and \(\omega\)
is the angular velocity.

\begin{equation}
	a = \frac{v^2}{r} = \omega^2 r
\end{equation}
Where \(a\) is centripetal acceleration.

\begin{equation}\label{centripetal_force}
	F = \frac{mv^2}{r} = m \omega ^ 2 r
\end{equation}
Where \(F\) is centripetal force.

You can investigate circular motion using a whirling bung.

\section{Oscillations}

\subsection{Simple Harmonic Oscillations}

For simple harmonic motion to apply, the acceleration on an object must be proportional to it's
displacement from a point of equilibrium and apply in the direction of equilibrium.

Angular frequency is conceptually the same thing as angular velocity in circular motion, and shares
the equations:

\begin{equation}
	\omega = \frac{2\pi}{T} = 2 \pi f
\end{equation}
Where \(\omega\) is the angular frequency of an oscillation, \(T\) is the
period of the oscillation, and \(f\) is the frequency of the oscillation.

For simple harmonic motion to apply, this equation must be met:

\begin{equation}
	a = - \omega ^ 2 x
\end{equation}
Where \(a\) is acceleration, \(\omega\) is angular frequency and \(x\) is
displacement.

The negative sign represents that the acceleration is in the opposite direction to the displacement.

Depending on whether the displacement-time graph starts at an amplitude of 0, this can rearrange to:

\begin{equation}
	x = A \cos (\omega t)
\end{equation}
or
\begin{equation}
	x = A \sin (\omega t)
\end{equation}

Where A is the amplitude of the oscillation, x is the displacement, and t is the time.

You can then calculate the derivative of this to find velocity-time and acceleration-time equations.

\begin{equation}
	v = \pm \omega \sqrt{A^2 - x^2}
\end{equation}
Where \(v\) is velocity, \(A\) is amplitude and \(x\) is displacement

The period of a simple harmonic oscillator is independent of its amplitude.

\subsection{Energy of a Simple Harmonic Oscillator}

During simple harmonic motion, there is an interchange of kinetic energy and potential energy with
displacement. However, the sum of kinetic energy and potential energy is always constant.

At minimum displacement, kinetic energy is at a maximum, and potential energy is 0. At maximum
displacement, potential energy is at that same maximum, and kinetic energy is 0.

Graphically, this looks like this:

\begin{center}
	\begin{center}
		\begin{tikzpicture}[scale=3, key/.style={circle, draw=black, fill=#1, inner sep=0pt, minimum size=3mm}]
			% Axes
			\draw[thick, ->, -latex] (0,0) -- (1,0) node[right] {Displacement};
			\draw[thick, ->, -latex] (0,0) -- (-1,0);
			\draw[thick,->, -latex] (0,0) -- (0,1) node[left] {Energy};

			% Potential energy
			\draw[thick, blue] plot [smooth, tension=1.1] coordinates {(-0.75, 0.75) (0, 0) (0.75, 0.75)};

			% Kinetic Energy
			\draw[thick, red] plot [smooth, tension=1.1] coordinates {(-0.75, 0) (0, 0.75) (0.75, 0)};

			% Total Energy
			\draw[thick, green] plot coordinates {(-0.75, 0.75) (0.75, 0.75)};
			\draw[thick, dashed] plot coordinates {(-0.75, 0) (-0.75, 0.75)};
			\draw[thick, dashed] plot coordinates {(0.75, 0) (0.75, 0.75)};

			% Labels
			\node[below] at (0,0) {0};
			\node[below] at (0.75,0) {A};
			\node[below] at (-0.75,0) {-A};

			% Key 
			\node[key=red, label=right:Kinetic Energy] at (-2,0.5) {};
			\node[key=blue, label=right:Potential Energy] at (-2,0.35) {};
			\node[key=green, label=right:Total Energy] at (-2,0.2) {};
		\end{tikzpicture}
	\end{center}
\end{center}

\subsection{Damping}

\subsubsection{Free and Forced oscillations}

When a mechanical system is displaced from equilibrium and then allowed to oscillate without any
external forces, its motion is called a free oscillation.

A forced oscillation is one in which a periodic driver force is applied to an oscillator.

In a forced oscillation, the object will oscillate at the frequency of the driving force (the
driving frequency).

\subsubsection{Effects of Damping}

For damping with exponential decrease, or exponential decay, the amplitude of the damping is divided by a fixed
constant over a given time period.

\subsubsection{Resonance}

Resonance occurs when the driving frequency of a forced oscillation is equal to the natural
frequency of the oscillating object.

The amplitude of an oscillation is considerably higher when it resonates, compared to when the
frequency is different.

As you increase damping on a forced oscillator, the amplitude of the oscillation decreases, and
becomes the driving frequency becomes increasingly less than the natural frequency of the forced
oscillation. The amplitude of the vibration at any frequency also decreases.

There are many usages of forced oscillations and resonance, such as how clocks use the resonance of
a pendulum or quartz crystal to keep time.

\section{Gravitational Fields}

\subsection{Point and Spherical Masses}

Gravitational fields are caused by objects having mass.

The mass of a spherical object can be modelled as being entirely centralized at its center.

You can use gravitational field lines to map gravitational fields. The field lines go in the
direction of an object's motion in that gravitational field.

The gravitational field strength at a point within a gravitational field is defined as the
gravitational force exerted per unit mass on a small object placed at that point within the field.

This can be written as

\begin{equation}\label{gravitational_field_strength}
	g = \frac{F}{m}	\quad (N {kg} ^ {-1})
\end{equation}

\subsection{Newton's law of gravitation}

To find the force acting between two point masses:

\begin{equation}\label{newton_gravitation}
	F = - \frac{GMm}{r^2}
\end{equation}

Where:

\begin{itemize}
	\item[\(F\)] = Force / N
	\item[\(G\)] = Universal Gravitational Constant / Nm\(^ 2\)kg\(^{-2}\)
	\item[\(M\)] = Mass of point mass 1 / kg
	\item[\(m\)] = Mass of point mass 2 / kg
	\item[\(r\)] = Distance between the point masses / m
\end{itemize}

The minus sign shows that the force is attractive.

Combining (\ref{gravitational_field_strength}) and (\ref{newton_gravitation}) gives:

\begin{equation}
	g = - \frac{GM}{r^2}
\end{equation}

Where:

\begin{itemize}
	\item[\(g\)] = Gravitational Field Strength / N kg\(^{-1}\)
	\item[\(G\)] = Universal Gravitational Constant / N m\(^ 2\) kg\(^{-2}\)
	\item[\(M\)] = Mass of point mass / kg
	\item[\(r\)] = Distance from the point mass / m
\end{itemize}

Close to the surface of the earth, the gravitational field strength is approximately uniform and
equal to approximately 9.81

\subsection{Planetary Motion}

Kepler has three laws of Planetary motion:

\begin{enumerate}
	\item The motion of a planet forms an ellipse, with the sun at one of it's two foci.
	\item A line segment joining a planet and the Sun sweeps out equal areas during equal intervals of time.
	\item The square of the orbital period \(T\) of a planet is directly proportional to the cube of
	      it's average distance \(r\) from the Sun, or \(T^2 \propto r^3\)
\end{enumerate}

Kepler's third law can be derived by setting (\ref{centripetal_force}) equal to
(\ref{newton_gravitation}) to find:

\begin{equation}
	T^2 = \left(\frac{4 \pi ^ 2}{GM}\right) r^3
\end{equation}

This can operate more generally to find the mass of an object from the period and radius of an
object in orbit around it.

\subsubsection{Satellite Orbits}

There are three main types of Earth orbit for satellites:

\begin{itemize}
	\item Equatorial orbit (orbiting around the equator)
	\item Polar orbit (orbiting around the line connecting the two poles)
	\item Low Earth orbit (orbiting elsewhere, low to the earth)
\end{itemize}

Geostationary satellites are Equatorial orbits. These satellites do not move in location compared to
a location directly below them on earth.

As a result, they have an orbital period of 24 hours.

\subsection{Gravitational Potential and Energy}

Gravitational potential at a point is defined as the work done to bring a unit mass from infinity to
said point. At infinity, gravitational potential is zero.

\begin{equation}
	V_g = - \frac{GM}{r}
\end{equation}

Where:

\begin{itemize}
	\item[\(V_g\)] = Gravitational Potential / J kg\(^{-1}\)
	\item[\(G\)] = Universal Gravitational Constant / N m\({^2}\) kg\(^{-2}\)
	\item[\(M\)] = Mass of the point mass causing the field / kg
	\item[\(r\)] = Distance to the point mass / m
\end{itemize}

For a force-distance graph for a spherical mass, the work done is the area under the graph.

\begin{equation}
	E = mV_g = - \frac{GMm}{r}
\end{equation}

Where:

\begin{itemize}
	\item[\(E\)] = Gravitational Potential Energy
	\item[\(m\)] = Mass of point mass in the gravitational field / kg
	\item[\(V_g\)] = Gravitational Potential / J kg\(^{-1}\)
	\item[\(G\)] = Universal Gravitational Constant / N m\(^2\) kg\(^{-2}\)
	\item[\(M\)] = Mass of the point mass causing the field / kg
	\item[\(r\)] = Distance to the point mass / m
\end{itemize}

Escape velocity is defined as the velocity required to travel from a point in a gravitational field
to infinity. It can be calculated using:

\begin{equation}
	v = \sqrt{\frac{2GM}{r}}
\end{equation}

\begin{itemize}
	\item[\(v\)] = Escape velocity / m s\(^{-1}\)
	\item[\(G\)] = Universal Gravitational Constant / N m\(^2\) kg\(^{-2}\)
	\item[\(M\)] = Mass of the point mass causing the field / kg
\end{itemize}

Note that escape velocity is independent of mass.

\section{Astrophysics and Cosmology}

\subsection{Stars}

You must be aware of the terms:

\begin{itemize}
	\item Planet - An object with enough mass to become spherical (which does not do nuclear fusion)
	\item Planetary Satellite - An object which orbits a planet
	\item Comet - A small irregular body made of ice, rock and dust
	\item Solar System - A planetary system consisting of a star and at least one planet in orbit around
	      it.
	\item Galaxy - A collection of stars and interstellar dust bound together by their mutual
	      gravitational force.
	\item The Universe
\end{itemize}

\subsubsection{Formation of a Star}

A star initially starts as a \textbf{nebula}, which is a large cloud of gas and dust, primarily made of
hydrogen.

Gravitational attraction pulls the gas and dust in the nebula together, this is known as
\textbf{gravitational collapse}.

This causes it the nebula to rotate, and as the particles get closer together, their gravitational
potential energy is transferred into kinetic energy. This causes \textbf{temperature} and
\textbf{pressure} to rise.

A star has been formed when it is hot enough and there is
enough pressure for \textbf{nuclear fusion} to occur, with hydrogen nuclei fusing together to form
helium.

The initial star formed is called a \textbf{protostar}.

\subsubsection{Transition to Main Sequence}

During the nuclear fusion process, four hydrogen nuclei are fused into a helium-4 nucleus, releasing
two neutrinos and two gamma ray photons, alongside two hydrogen nuclei.

The momentum of the gamma ray photons results in an outward acting pressure known as
\textbf{radiation pressure}.

After formation, a protostar expands, as the inwards force due to gravity is less than the outwards
radiation pressure emitted by the star, causing it to expand.

A star reaches the main sequence once it's inwards pressure due to gravity is equal to it's outwards
pressure due to the star's radiation - I.E. the star is in equilibrium.

\subsubsection{Low Mass Stars}

A low mass star is defined as a star between 0.5 and 10 solar masses (meaning 0.5 to 10 times the
mass of the sun).

Once the hydrogen fuelling the star runs out, nuclear fusion stops. This causes the star to
contract, and the core of the star to collapse, as the radiation pressure of the star decreases.

Hydrogen fusion continues in the shell around the core, and the core of the star collapses enough
for it to become a high enough temperature and pressure to cause helium to fuse into carbon and
oxygen.

The outer layers of the star expand and cool, forming a red giant.

The internal helium burning causes the core to increase in temperature and pressure, increasing the
outward radiation pressure. Once the helium runs out, the core contracts again, generating
temperatures high enough to fuse the helium in the shell around the core.

The carbon-oxygen core is not hot enough to fuse the heavy elements, causing the star to collapse
again. The outer layers of gas are ejected back into space, forming a planetary nebula.

The solid core becomes a very hot, dense core called a white dwarf. The white dwarf eventually cools
to a few degrees kelvin, at which point it emits no light or heat, and becomes a black dwarf.

\subsubsection{Differences Between a Low and High Mass Star}

When matter is compressed into a very small volume, like it is when the core of a star collapses,
the electrons are no longer able to freely move between energy levels, and are forced to fill the
available energy levels.

This causes electrons to fill the higher energy levels, when they do not have sufficient energy to
do so normally. The energy required for the electrons to do this creates an outwards pressure, known
as electron degeneracy pressure.

\subsubsection{The Chandrasekhar Limit}

The Chandrasekhar limit is the maximum mass of a stable white dwarf. It is equal to 1.4 times the
mass of the sun. If a core's mass exceeds this limit, it will become a neutron star or a black hole,
instead of a white dwarf, as the electron degeneracy pressure will not be enough to prevent a core
from collapsing in on itself.

\subsubsection{High Mass Stars}

High mass stars follow the same initial process as low mass stars, except they form a red supergiant
instead of a red giant.

The core burning in a red supergiant goes beyond that of a red giant, fusing elements all the way up
to iron, eventually causing an iron core to form.

This iron core collapses in on itself, as no more fusion reactions can occur. The gravitational
potential energy transferred in the collapse causes intense heating, and the intense pressure in the
core causes protons and electrons to combine into neutrons.

The outer shell of the star falls inwards on the core, and bounces off, causing shockwaves which
cause the outer layer to be fired off in a supernova. This supernova has a high enough temperature
to fuse heavy nuclei with neutrons to form the elements heavier than iron.

If the resulting neutron core has mass less than 3 times the mass of the sun, it remains intact. If
it is greater than 3 times the mass of the sun, the pressure on the core is so great that it
collapses even further, until the escape velocity of the core is greater than the speed of light,
forming a black hole.

\subsubsection{Neutron Stars and Black Holes}

Some neutron stars rotate rapidly, emitting bursts of electromagnetic radiation in a given
direction, known as pulsars.

The core of a black hole collapses into an infinitely dense point known as a singularity. The
gravitational field is so strong that light cannot escape it.

The boundary at which light cannot escape the gravity of a black hole is known as the event horizon.

\subsubsection{Hertzsprung-Russell Diagram}

The Hertzsprung-Russel Diagram plots luminosity against temperature for stars. A Hertzsprung Russell
diagram is shown here:

\begin{figure}[htbp]
	\centering
	\includegraphics[width=\textwidth]{assets/hertzsprung-russell-diagram.png}
	\caption{A Hertzsprung Russell Diagram}
	\label{Hertzsprung Russell Diagram}
\end{figure}

Note the different "clusters" for white dwarfs, red supergiants, red giants and main sequence stars.

\subsection{Electromagnetic Radiation from Stars}

The energy levels of electrons in isolated gas atoms are quantised (i.e. they can only have certain
values).

When an electron emits or absorbs energy, it will be because it has changed energy level between
these defined states.

Energy levels have negative values, with the highest energy level of an electron being 0 - which is
when it has escaped the atom.

\subsubsection{Emission Spectra}

When a gas is heated up a large amount, it emits light. The light emitted appears to be a uniform
colour, but can be split up into a variety of light spectra using a diffraction grating.

This emission spectrum will have very distinct lines at certain wavelengths/frequencies. This is
because the photons released have quantised energy levels, due to the quantisation of energy
released by electrons moving between energy levels.

\begin{equation}
	\Delta E = hf = \frac{hc}{\lambda}
\end{equation}

\begin{itemize}
	\item[\(\Delta E\)] = Change in energy level of an electron / \unit{\joule}
	\item[\(h\)] = Plank's Constant / \unit{\joule \second}
	\item[\(f\)] = Emitted Photon Frequency / \unit{\hertz}
	\item[\(c\)] = Speed of Light / \unit{\meter \per \second}
	\item[\(\lambda\)] = Wavelength of Emitted Photon / \unit{\meter}
\end{itemize}

Different elements have different spectral lines, allowing us to identify the elements in stars from
the wavelengths of light that they emit.

Stars show an absorption spectrum, rather than an emission spectrum. This is because the core of the
star emits a continuous spectrum, and the outer layers of the star then absorb certain wavelengths
of light, meaning that what you see is a continuous spectrum with lines of darkness at certain
frequencies.

\subsubsection{Diffraction Grating}

A diffraction grating can be used to determine the wavelength of light emitted. When a monochromatic
stream of light is passed through, a number of "dots" can be seen on a screen placed in the path of
the light. This is due to the interference pattern of the light emitted.

To calculate the wavelength:

\begin{equation}
	d \sin \theta = n \lambda
\end{equation}

Where:

\begin{itemize}
	\item[\(d\)] = Grating Spacing / \unit{\meter}
	\item[\(\theta\)] = Angle from the Grating to a Maxima / \unit{\radian}
	\item[\(n\)] = Order of the Maxima
	\item[\(\lambda\)] = Wavelength of the Light / \unit{\meter}
\end{itemize}

\subsubsection{Wein's Displacement Law}

Wein's Displacement Law can be used to calculate the peak surface temperature of a star. It states
that the black body radiation curve for different temperatures peaks at a wavelength which is
inversely proportional to the temperature.

\begin{equation}
	\lambda_{\max} \propto \frac{1}{T}
\end{equation}

Where:

\begin{itemize}
	\item[\(\lambda_{\max}\)] = The wavelength emitted by the star with the highest intensity /
		\unit{\meter}
	\item[\(T\)] = Temperature of the Star / \unit{\kelvin}
\end{itemize}

\subsubsection{Luminosity and Stefan's Law}

\textbf{Luminosity} is defined as the total output of radiation emitted by a star. It is measured in
Watts (\unit{\watt}).

The luminosity of a star can be calculated using Stefan's law, which states that:

\begin{equation}
	L = 4 \pi r ^ 2 \sigma T^4
\end{equation}

Where:

\begin{itemize}
	\item[\(L\)] = Luminosity / \unit{\watt}
	\item[\(r\)] = Radius of the Star / \unit{\meter}
	\item[\(\sigma\)] = The Stefan-Boltzmann constant / \unit{\watt \meter ^{-2} \kelvin ^ {-4}}
	\item[\(T\)] = Temperature of the Star / \unit{\kelvin}
\end{itemize}

\subsection{Radiant Flux and Estimating The Radius of a Star}

The radiant flux intensity at a point can be calculated using the following formula:

\begin{equation}
	F = \frac{L}{4 \pi d ^ 2}
\end{equation}

Where:

\begin{itemize}
	\item[\(F\)] = Radiant Flux intensity / \unit{\watt \per \meter \squared}
	\item[\(L\)] = Luminosity of the Star / \unit{\watt}
	\item[\(d\)] = Distance to the star / \unit{\meter}
\end{itemize}

This can be used in combination with Wein's Displacement law and Stefan's Law to estimate the radius
of a star, by using Wein's law to calculate the temperature of the star, and rearranging Stefan's
law to calculate radius.

\subsection{Cosmology}

\subsubsection{Units for Astronomical Distances}

Astronomical distances are measured using the following units:

\begin{itemize}
	\item Astronomical Units (AU) = The mean distance from the centre of the Earth to the centre of the
	      Sun.
	\item Light-years (ly) = The distance travelled by light in a year.
	\item Parsecs (pc) = A unit of distance that gives a parallax angle of 1 second of an arc (of a degree), using the
	      radius of the earth's orbit as the baseline of a right-angled triangle.
\end{itemize}

\subsubsection{Stellar Parallax}

\textbf{Stellar Parallax} is defined as the apparent shifting in position of a nearby star against a
background of distant stars when viewed from different positions of the Earth, during the Earth's
orbit about the sun.

\begin{equation}
	p = \frac{1}{d}
\end{equation}

Where:

\begin{itemize}
	\item[\(p\)] = Parallax / arc seconds
	\item[\(d\)] = Distance to the Star / pc
\end{itemize}

Note that the arc minute represents \(\frac{1}{60}\) of a degree, and the arc second is
\(\frac{1}{60}\) of an arc minute.

This angle is measured at the two points in the earth's orbit where a line connecting those points
is perpendicular to a line connecting the star and the sun.


Note that this equation becomes inaccurate for distances above 100 pc, as the angles involved are so
small they become hard to measure accurately.

\subsubsection{The Cosmological Principle}

The cosmological principle states that the universe is \textbf{isotropic}, \textbf{homogenous} and
the laws of physics are \textbf{universal}.

\begin{itemize}
	\item \textbf{Isotropic} means the universe is the same in all directions to every observer.

	\item \textbf{Homogenous} means that matter is uniformly distributed.
	\item \textbf{The Laws Of Physics are Universal} means that the same laws and models apply at any point in the universe.
\end{itemize}

\subsubsection{The Doppler Effect}

The \textbf{doppler effect} describes the apparent shift in wavelength of a wave due to the
velocity of the wave's source.

The Doppler effect can be observed using any form of electromagnetic radiation, by comparing the
light spectrum produced by a close object, e.g. the sun, with that of a distant galaxy, where the
emitted light from the distant galaxy is "red shifted" due to the expansion of the universe.

\begin{equation}
	\frac{\Delta \lambda}{\lambda} \approx \frac{\Delta f}{f} \approx \frac{v}{c}
\end{equation}

Where:

\begin{itemize}
	\item[\(\Delta \lambda\)] = Apparent change in wavelength / \unit{\meter}
	\item[\(\lambda\)] = True wavelength / \unit{\meter}
	\item[\(v\)] = Velocity of source relative to observer/ \unit{\meter \per \second}
	\item[\(c\)] = Speed of light / \unit{\meter \per \second}
\end{itemize}

\subsubsection{Hubble's Law}

Hubble's law states that the recessional velocity, v, of a galaxy relative to an observer is
proportional to it's distance from the observer.

\begin{equation}
	v \approx H_0 d
\end{equation}

Where:

\begin{itemize}
	\item[\(v\)] = Recessional velocity of an object / \unit{\kilo \meter}
	\item[\(H_0\)] = The Hubble Constant / \unit{\kilo \meter \per \second \per \mega \parsec }
	\item[\(d\)] = Distance to the object / \unit{\mega \parsec}
\end{itemize}

Note that the Hubble Constant's units can change. Other common units include \unit{\per \second}.

\subsubsection{Red Shift}

There is an observed red shift in electromagnetic radiation observed from far away galaxies. This
implies that those galaxies are travelling away from us, as the doppler effect must be causing this
red shift.

This red shift is proportional to the distance of that galaxy from us.

This provides evidence for the expansion of the universe, as this red shift is observed at all
points in the universe.

\subsubsection{The Big Bang theory}

The big bang theory states that around 7 billion years ago, the universe was created from a hot
singularity, which was infinitely dense, hot and small.

There was an explosion, known as the Big Bang, in which both space and time were created.

This caused the universe to expand and cool from a single point.

\subsubsection{Evidence for the Big Bang and CMBR}

Hubble's Law shows the universe is expanding, through the red shift of distance galaxies.

The strongest piece of evidence for the big bang is the cosmological microwave background radiation.

There is an observed microwave radiation acting from all directions at a generally uniform
wavelength and temperature.

The big bang theory states that this radiation came from the thermal radiation of the hot and dense
early universe. This radiation has expanded with the expansion of the universe, and is now in the
microwave region of the EM spectrum.

This microwave radiation has now cooled to a temperature of 2.7k. The wavelength and temperature of
this background radiation can be used to corroborate the age of the universe, of around 13.7 billion years.

\subsubsection{Space Time}

Einstein's theory of general relativity connects the three dimensions of space to a fourth
dimension, which is time.

The idea states that instead of the objects in the universe expanding, space itself is expanding.
This is evidenced by the fact that regardless of where you are in space, all other galaxies appear
to be moving away from one another by the same amount.

This agrees with the cosmological principle, which states that the universe is homogeneous.

\subsubsection{Estimation of the Age of the Universe}

The age of the universe can be calculated with the following formula:

\begin{equation}
	t \approx {H_0} ^{-1}
\end{equation}

Where:

\begin{itemize}
	\item[\(t\)] = Age of the Universe / \unit{\second}
	\item[\(H_0\)] = The Hubble Constant / \unit{\per \second}
\end{itemize}

Note that this means the Hubble Constant is not actually a constant, and it varies over time.

\subsubsection{Evolution Of the Universe}

\todo

\subsubsection{Dark Energy and Dark Matter}

Dark Energy is a hypothetical
