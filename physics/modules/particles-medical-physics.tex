\chapter{Particles and Medical Physics}

\section{Capacitors}

\subsection{Capacitors}

\begin{equation}
	C = \frac{Q}{V}
\end{equation}

Where:

\begin{itemize}\label{Capacitance}
	\item[\(C\)] = Capacitance / \unit{\farad}
	\item[\(Q\)] = Charge / \unit{\coulomb}
	\item[\(V\)] = Potential Difference / \unit{\volt}
\end{itemize}

\subsubsection{Charging}

When a capacitor is charging, electrons flow from the positive plate of the capacitor to the
positive terminal on the cell of the battery. Electrons also then flow from the negative side of the
cell to the negative plate of the capacitor.

This flow decreases with respect to time, as the P.D. between the plates and the cell decreases as
electrons build up on the plates.

\subsubsection{Discharging}

When a capacitor is discharging, electrons flow from the negative plate of the capacitor through the
circuit that the capacitor is connected to, and flow towards the positive plate.

This causes the number of surplus electrons on the positive plate to decrease, and the number of
absent electrons on the positive plate to increase, causing the P.D. across the capacitor to>
decrease with respect to time.

\subsubsection{Capacitors in Series}

\begin{equation}
	\frac{1}{C_t} = \frac{1}{C_1} + \frac{1}{C_2} + \frac{1}{C_3} + \dots
\end{equation}

Where:

\begin{itemize}
	\item[\(C_t\)] = Total capacitance / \unit{\farad}
	\item[\(C_n\)] = Capacitance of capacitor \(n\) in the series circuit / \unit{\farad}
\end{itemize}

\subsection{Capacitors in Parallel}

\begin{equation}
	C_t = C_1 + C_2 + C_3 + \dots
\end{equation}

\begin{itemize}
	\item[\(C_t\)] = Total capacitance / \unit{\farad}
	\item[\(C_n\)] = Capacitance of capacitor \(n\) in the parallel circuit / \unit{\farad}
\end{itemize}

\subsection{Energy}

For a capacitor, you can plot a graph with potential difference on the y, and charge on the x. The
area under the graph will be equal to the energy stored by the capacitor.

\begin{equation}
	W = \frac{1}{2}QV = \frac{Q^2}{2C} = \frac{1}{2}V^2C
\end{equation}

Where:

\begin{itemize}
	\item[\(W\)] = Work Done / \unit{\joule}
	\item[\(Q\)] = Charge / \unit{\coulomb}
	\item[\(V\)] = Potential Difference / \unit{\volt}
	\item[\(C\)] = Capacitance / \unit{\farad}
\end{itemize}

\subsection{Charging and Discharging Capacitors}

A common circuit for charging/discharging capacitors is as follows:

\begin{center}
	\begin{circuitikz}[scale=1.5]
		\draw (0,0) to[nos, l=$S_1$] (2,0) to[capacitor, l=$C$] (2,-2) -- (0,-2) to[battery, l = $V$] (0,0);
		\draw (2,0) to[nos, l=$S_2$] (4,0) to[european resistor, l=$R$] (4,-2) -- (2,-2);
	\end{circuitikz}
\end{center}

When $S_1$ is closed, and $S_2$ is open, the capacitor charges.

When $S_1$ is open, and $S_2$ is closed, the capacitor discharges through the resistor $R$.

\subsubsection{Time Constant}

\begin{equation}
	\tau = CR
\end{equation}

Where:

\begin{itemize}
	\item[\(\tau\)] = Time Constant / \unit{\second}
	\item[\(C\)] = Capacitance / \unit{\farad}
	\item[\(R\)] = Resistance / \unit{\ohm}
\end{itemize}

\subsubsection{Equations for Charging/Discharging}

\begin{equation}\label{capacitor1}
	x = x_0e^{-\frac{t}{CR}}
\end{equation}

and

\begin{equation}\label{capacitor2}
	x = x_0 ( 1 - e^{-\frac{t}{CR}})
\end{equation}

Where:

\begin{itemize}
	\item[\(x\)] = Potential Difference / \unit{\volt} or Current / \unit{\ampere}
	\item[\(x_0\)] = Initial Potential Difference / \unit{\volt} or Current / \unit{\ampere}

	\item[\(t\)] = Time / \unit{\second}
	\item[\(C\)] = Capacitance / \unit{\farad}
	\item[\(R\)] = Resistance / \unit{\ohm}
\end{itemize}

To determine which of the two equations to use, think about whether the value you are measuring will
be increasing or decreasing. If it's increasing, use equation \ref{capacitor1}, else use equation
\ref{capacitor2}.

\subsubsection{Graphical Modelling of a Capacitor}

To calculate the time constant of a capacitor, plot a graph of \(\ln(V)\) on the y and \(t\) on the
x.

The gradient of this graph will be equal to \(-\frac{1}{\tau}\), so to find \(\tau\) use \(\tau = -
\frac{dx}{dy}\)

\section{Electric Fields}

\subsection{Point and Spherical Charges}

Electric fields are caused by the charge of an object.

A uniformly charged sphere is modelled as a point charge at that sphere's center.

To map electric fields, electric field lines are used. An electric field diagram looks like this:

\begin{center}
	\begin{tikzpicture}[scale=1]
		% Draw the electric field lines
		\foreach \i in {1,...,12}
		\draw[thick, ->] ({\i*30}:0.75) -- ({\i*30}:2.0);

		% Draw the electron
		\draw[black] (0, 0) circle (0.75) node {\huge{+}};
	\end{tikzpicture}
\end{center}


Note that the lines are evenly spaced, and that they travel in the direction that a positive charge would travel in their location.

\subsubsection{Electric Field Strength}

\begin{equation}
	E = \frac{F}{Q}
\end{equation}

Where:

\begin{itemize}\label{F=EQ}
	\item[\(E\)] = Electric Field Strength / \unit{\newton \per \coulomb}
	\item[\(F\)] = Force due to the Electric Field / \unit{\newton}
	\item[\(Q\)] = Charge / \unit{\coulomb}
\end{itemize}

\subsection{Coulomb's Law}

The force acting between two point charges can be calculated using the following formula:

\begin{equation}
	F = \frac{Qq}{4\pi \varepsilon_0r^2}
\end{equation}

Where

\begin{itemize}\label{Coulomb's law}
	\item[\(F\)] = Force between the two point charges / \unit{\newton}
	\item[\(Q\)] = The charge on the one of the point charges / \unit{\coulomb}
	\item[\(q\)] = The charge on the other point charge / \unit{\coulomb}
	\item[\(\varepsilon_0\)] = The permittivity of free space / \unit{\farad \per \meter}
	\item[\(r\)] = The distance between the two point charges / \unit{\meter}
\end{itemize}

Combining \ref{F=EQ} and \ref{Coulomb's law} we get:

\begin{equation}
	E = \frac{Q}{4\pi \varepsilon_0r^2}
\end{equation}

Where:

\begin{itemize}
	\item[\(E\)] = Electric Field Strength / \unit{\newton \per \coulomb}
	\item[\(Q\)] = The charge on a point charges / \unit{\coulomb}
	\item[\(\varepsilon_0\)] = The permittivity of free space / \unit{\farad \per \meter}
	\item[\(r\)] = The distance from the point charge / \unit{\meter}
\end{itemize}

\subsection{Uniform Electric Fields}

For a uniform electric field between two plates:

\begin{equation}
	E = \frac{V}{d}
\end{equation}

Where:

\begin{itemize}
	\item[\(E\)] = Electric Field Strength / \unit{\newton \per \coulomb}
	\item[\(V\)] = Potential difference across the two plates / \unit{\volt}
	\item[\(d\)] = Distance between the two plates / \unit{\meter}
\end{itemize}

\subsubsection{Parallel Plate Capacitor}

\begin{equation}
	C = \frac{\varepsilon_0 A}{d}
\end{equation}

Where:

\begin{itemize}
	\item[\(C\)] = Capacitance / \unit{\farad}
	\item[\(\varepsilon_0\)] = Permittivity of Free Space / \unit{\farad \per \meter}
	\item[\(A\)] = Cross Sectional Area of the Plates / \unit{\meter \squared}
	\item[\(d\)] = Distance between the two plates / \unit{\meter}
\end{itemize}

For a parallel plate capacitor with some form of insulator between the plates (instead of just air):

\begin{equation}
	C = \frac{\varepsilon A} {d}
\end{equation}

Where:

\begin{itemize}
	\item[\(C\)] = Capacitance / \unit{\farad}
	\item[\(\varepsilon\)] = Permittivity of the Insulator/ \unit{\farad \per \meter}
	\item[\(A\)] = Cross Sectional Area of the Plates / \unit{\meter \squared}
	\item[\(d\)] = Distance between the two plates / \unit{\meter}
\end{itemize}

We can calculate \(\varepsilon\) from the following equation:

\begin{equation}
	\varepsilon = \varepsilon_r \varepsilon_0
\end{equation}

Where:

\begin{itemize}
	\item[\(\varepsilon\)] = Permittivity for the insulator / \unit{\farad \per \meter}
	\item[\(\varepsilon_r\)] = Relative Permittivity of the Insulator (unitless)
	\item[\(\varepsilon_0\)] = Permittivity of Free Space / \unit{\farad \per \meter}
\end{itemize}

\subsection{Electric Potential and Energy}

\subsubsection{Electric Potential}

The electric potential at a point is the work done to bring a unit positive charge from a point at
infinity to the point. Electric potential at infinity is 0.

\begin{equation}\label{Electric Potential}
	V = \frac{Q}{4 \pi \varepsilon_0 r}
\end{equation}

Where:

\begin{itemize}
	\item[\(V\)] = Electric Potential / \unit{\volt}
	\item[\(Q\)] = Charge on the Point Charge / \unit{\coulomb}
	\item[\(r\)] = Distance from the Point Charge / \unit{\meter}
\end{itemize}

For an isolated sphere:

\begin{equation}
	C = 4 \pi \varepsilon_0 r
\end{equation}

Which can be derived from \ref{Capacitance} and \ref{Electric Potential}.

\subsubsection{Electric Potential Energy}

From \ref{Electric Potential} and \(E=Vq\) it is possible to derive:

\begin{equation}
	E_{EP} = \frac{Qq}{4 \pi \varepsilon_0 r}
\end{equation}

\section{Electromagnetism}

\subsection{Magnetic Fields}

Magnetic fields can be generated in two ways:

\begin{enumerate}
	\item Motion of charges
	\item Permanent magnets
\end{enumerate}

\subsubsection{Magnetic Field Lines}

We use magnetic field lines to map magnetic fields.

Magnetic field lines go from the north pole of a magnet to the south pole of the magnet.

\todo{Cook up diagrams for electric field lines}

\subsubsection{Fleming's Left Hand Rule}

We use fleming's left hand rule to determine the direction of the force applied due to an
electromagnetic field.

Flemings left hand rule is shown in this image:

\begin{figure}[H]
	\centering
	\includegraphics[width=100mm]{assets/fleming-left-hand.jpeg}
	\caption{Fleming's left hand rule.}
	\label{fleming-left-hand}
\end{figure}

\subsubsection{Determining the force acting on a current carrying conductor}

\begin{equation}
	F = BIL \sin(\theta)
\end{equation}

Where:

\begin{itemize}
	\item[\(F\)] = Force / \unit{\newton}
	\item[\(B\)] = Magnetic Flux Density / \unit{\tesla}
	\item[\(I\)] = Current / \unit{\ampere}
	\item[\(L\)] = Length of the Wire in the Magnetic Field / \unit{\meter}
	\item[\(\theta\)] = The angle between the Magnetic Field and the Wire / \unit{\degree}
\end{itemize}

Magnetic Flux Density is defined as the strength of the magnetic field, or as the force applied per
unit current per unit perpendicular length.

\subsection{Motion of Charged Particles}

When a charged particle is travelling at a right angle to a uniform magnetic field:

\begin{equation}
	F = BQv
\end{equation}

Where:

\begin{itemize}
	\item[\(F\)] = Force on a charged particle / \unit{\newton}
	\item[\(B\)] = Magnetic flux density / \unit{\tesla}
	\item[\(Q\)] = Charge on the particle / \unit{\coulomb}
	\item[\(v\)] = Velocity of the particle / \unit{\meter \per \second}
\end{itemize}

This force acts always perpendicular to the direction of motion, resulting in circular motion.

This results in

\begin{equation}
	BQv = \frac{mv^2}{r} \implies r = \frac{mv}{BQ}
\end{equation}

\subsubsection{Velocity Selector}

Velocity Selectors allow particles of only a particular velocity to pass through. They function by
passing charged particles through a narrow chamber, which has a potential difference across it as
well as and a magnetic field within it.

For a particle to remain undeflected (and thus pass through the chamber), it's electric force must
equal it's magnetic force, hence:

\begin{equation*}
	EQ = BQv \implies v = \frac{E}{B}
\end{equation*}

Thus, only particles of a certain speed will pass through.

\subsection{Electromagnetism}

Magnetic flux is defined as the product of the component of the magnetic flux density perpendicular
to the area and the cross-sectional area, or:

\begin{equation}
	\phi = BA \cos \theta
\end{equation}

Where:

\begin{itemize}
	\item[\(\phi\)] = Magnetic Flux / \unit{\weber}
	\item[\(B\)] = Magnetic Flux Density / \unit{\tesla}
	\item[\(A\)] = Area / \unit{\meter \squared}
	\item[\(\theta\)] = Angle between the area and the direction of the magnetic field
\end{itemize}

\subsubsection{Magnetic Flux Linkage}

\begin{equation}
	\varPhi = N \phi
\end{equation}

Where:

\begin{itemize}
	\item[\(\varPhi\)] = Magnetic Flux Linkage / \unit{\weber}
	\item[\(N\)] = Number of Turns in the Coil
	\item[\(\phi\)] = Magnetic Flux / \unit{\weber} 
\end{itemize}

Note that flux linkage is occasionally given the unit "weber-turns" to differentiate it from
magnetic flux.

\subsubsection{Faraday's Law of Electromagnetic Induction}

Faraday's law states that the magnitude of the induced e.m.f. is directly proportional to the rate
of change of magnetic flux linkage, or:

\begin{equation*}
    \varepsilon \propto \frac{\Delta \varPhi}{\Delta t}
\end{equation*}

\subsubsection{Lenz's Law}

Lenz's law states that the direction of the induced e.m.f or current is always such as to oppose the
change causing it.

Integrating Lenz's law into Faraday's law, we get:

\begin{equation}
    \varepsilon = - \frac{\Delta \varPhi}{\Delta t}
\end{equation}

Where:

\begin{itemize}
    \item[\(\varepsilon\)] = Induced e.m.f. / \unit{\volt}
    \item[\(\varPhi\)] = Flux Linkage / \unit{\weber}
    \item[\(t\)] = Time / \unit{\second}  
\end{itemize}

\section{Nuclear and Particle Physics}

\subsection{The Nuclear Atom}

\subsubsection{Alpha Scattering Experiment}

The Alpha Scattering experiment bombarded a sheet of gold leaf with alpha particles, with a geiger
molletube used to determine the motion of the alpha particles after colliding with the sheet.

Firstly, most of the particles passed straight through the gold, and were undeflected. This gave
rise to the idea that most of the atom was empty space.

Some of the alpha particles were deflected. This gave rise to the idea that charge was centralised
in the atom, as if charge was evenly distributed the alpha particles would not be deflected.

Some of the alpha particles were reflected. This gave rise to the idea that mass was also
centralised in the atom, as the alpha particles must have been colliding with some small but dense
object to be reflected.

This experiment gave rise to the concept of the "nucleus".

\subsubsection{Size of the Atom/Nucleus}

\todo

\subsubsection{Representation of Nuclei}

Each nucleus has two significant numbers: the proton number, and the nucleon number. The proton
number dictates how many protons are in the nucleus, and the nucleon number dictates how many
protons and neutrons are in the nucleus.

Each element has a number of isotopes, which are nuclei with the same number of protons but a
different number of neutrons/nucleons.

Nuclei can be represented using the following notation:

\[\ce{^A_ZX}\]

Where:

\begin{itemize}
	\item[\(A\)] = Nucleon Number
	\item[\(Z\)] = Proton Number
	\item[\(X\)] = Element Symbol  
\end{itemize}

\subsubsection{The Strong Nuclear Force}

The strong nuclear force (often referred to as just the strong force) is a force that occurs between
hadrons. The force is strongly attractive between 3\unit{\femto \meter} and 0.5\unit{\femto \meter}.
Below 0.5\unit{\femto \meter} the strong force is extremely repulsive, and beyond 3\unit{\femto
\meter} it's effect is negligible.

\subsubsection{Radius of a Nucleus}

The radius of a nucleus can be calculated using the following formula:

\begin{equation}
	R = r_0 A^{\frac{1}{3}}
\end{equation}

Where:

\begin{itemize}
	\item[\(R\)] = Radius / \unit{\meter}
	\item[\(r_0\)] = A constant
	\item[\(A\)] = Nucleon Number  
\end{itemize}

\subsubsection{Mean Densities of Atoms and Nuclei}

\todo

\subsection{Fundamental Particles}

Every particle has a respective anti-particle: electron-positron, proton-antiproton,
neutron-antineutron and neutrino-antineutrino.

Each antiparticle has the same mass as it's respective particle, with the opposite charge.

\subsubsection{Hadrons}

The term "hadron" refers to any particle that is made up of quarks. Hadrons can be split up in to
two sub-categories:

\begin{itemize}
	\item Baryons - 3 quarks.
	\item Mesons - A quark and an antiquark.
\end{itemize}

Protons and Neutrons are both examples of hadrons, and are more specifically Baryons. 

All Hadrons experience both the strong and weak nuclear force.

\subsubsection{Leptons}

The term "lepton" refers to particles that are not made up of quarks. Examples of leptons include
electrons, positrons, neutrinos and antineutrinos.

All leptons experience the weak nuclear force but not the strong nuclear force.

\subsubsection{Quarks}

On the A level course, there are three types of quark to know about:

\begin{itemize}
	\item Up ($\quarku$)
	\item Down ($\quarkd$)
	\item Strange ($\quarks$)
\end{itemize}

You must also know about their respective antiquarks:

\begin{itemize}
	\item Anti-up ($\antiquarku$)
	\item Anti-down ($\antiquarkd$)
	\item Anti-strange ($\antiquarks$)
\end{itemize}

The quarks have the following charges:

\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|}
		\hline
		\multicolumn{3}{|c|}{Quarks} & \multicolumn{3}{c|}{Anti-Quarks} \\
		\hline 
		Name & Symbol & Charge & Name & Symbol & Charge \\
		\hline
		up & $\quarku$ & \(\frac{2}{3}e\) & anti-up & $\antiquarku$ & \(-\frac{2}{3}e\) \\
		down & $\quarkd$ & \(-\frac{1}{3}e\) & anti-down & $\antiquarkd$ & \(\frac{1}{3}e\) \\
		strange & $\quarks$ & \(- \frac{1}{3}e\) & anti-strange & $\antiquarks$ & \(\frac{1}{3}e\) \\
		\hline
	\end{tabular}
\end{center}

\subsubsection{Protons and Neutrons}

Protons are formed of two up quarks and a down quark, often represented as $uud$

Neutrons are formed of an up quark and two down quarks, represented as $udd$

\subsubsection{Beta Minus ($\beta ^-$) Decay}

Beta minus decay occurs when a down quark decays into an up quark, releasing an electron and an
anti-neutrino.

This overall causes a neutron to decay into a proton, an electron and an anti-neutrino.

Symbolically:

\[\quarkd \rightarrow \quarku + \ce{^0_{-1}e} + \antineutrino\]

\subsubsection{Beta Plus ($\beta^+$) Decay}

Beta plus decay occurs when an up quark decays into a down quark, releasing a positron and a
neutrino.

This overall causes a proton to decay into a neutron, a positron and a neutrino.

Symbolically:

\[\quarku \rightarrow \quarkd + \ce{^0_{+1}e} + \neutrino\]

Note that in a quark equation, charge must be conserved.

\subsection{Radioactivity}

Radioactive decay is \textbf{spontaneous} and \textbf{random}. 

\textbf{Spontaneous} means that it is not effected by external factors, such as the presence of
other nuclei, or pressure.

\textbf{Random} means that we cannot predict when a particular nucleus in a sample will decay or
which one will decay next.

\subsubsection{Radioactive Particles}

There are three types of radioactive particle:

\begin{itemize}
	\item $\alpha$ particles - Two protons, two neutrons - a helium nucleus.
	\item $\beta$ particles - An electron or positron.
	\item $\gamma$ rays - A photon.
\end{itemize}

Alpha particles are extremely ionising, however they have a very short range. They cannot penetrate
a sheet of paper.

Beta particles are less ionising than alpha particles, but have a longer range. They can penetrate a
sheet of paper, but cannot penetrate 5\unit{\milli\meter} of aluminium.

Gamma rays are even less ionising than beta particles, but have an even longer range. They can
penetrate 5\unit{\milli\meter} of aluminium, but cannot penetrate 5\unit{\centi\meter} of lead.

\subsubsection{Activity and Decay}

\begin{equation}
	A = \lambda N
\end{equation}

Where:

\begin{itemize}
	\item[\(A\)] = Activity
	\item[\(\lambda\)] = Decay Constant 
	\item[\(N\)] = The Number of Nuclei in the Sample 
\end{itemize}

The activity of the source can also be written as \(\frac{\Delta N}{\Delta t}\).

\subsubsection{Decay Constant}

The decay constant for a material is defined as the probability of decay of an individual nucleus
per unit time.

\subsubsection{Half-life}

The half-life of an isotope is the amount of time required for the number of samples of that isotope
to halve due to decay.

The decay constant and half life are related by the equation:

\begin{equation}
	\lambda t_{\frac{1}{2}} = \ln(2)
\end{equation}

Where:

\begin{itemize}
	\item[\(\lambda\)] = Decay constant / \unit{\per \second}
	\item[\(t_{\frac{1}{2}}\)] = Half life / \unit{\second}
\end{itemize}

\subsubsection{Modelling Radioactivity}

\begin{equation}
	A = A_0 e ^ {-\lambda t}
\end{equation}

Where:

\begin{itemize}
	\item[\(A\)] = Activity
	\item[\(A_0\)] = Initial Activity
	\item[\(\lambda\)] = Decay Constant / \unit{\per \second}
	\item[\(t\)] = Time / \unit{\second}
\end{itemize}

\subsubsection{Carbon Dating}

The ratio of carbon-14 to carbon-12 nuclei remains reasonably constant. Once an organism dies, it
stops taking in new carbon, so the ratio of carbon-14 to carbon-12 slowly decreases. We can use this
to calculate the time since an organism dies.

\subsection{Nuclear Fission and Fusion}

\subsubsection{Einstein's Energy-Mass Equation}

\begin{equation}\label{Einstein's Energy-Mass Equation}
	\Delta E = \Delta mc^2
\end{equation}

Where

\begin{itemize}
	\item[\(\Delta E\)] = Change in Energy / \unit{\joule}
	\item[\(\Delta m\)] =  Change in Mass / \unit{\kilogram}
	\item[\(c\)] = Speed of Light / \unit{\meter \per \second} 
\end{itemize}

In nuclear reactions, some amount of energy is released, or taken in. This energy change can be
calculated using the change in total mass of the system, using Einstein's energy-mass equation.

\subsubsection{Particle-Antiparticle pairs}

When a particle meets it's respective antiparticle, e.g. an electron meets a positron, they
annihilate one another. This means that the particle is entirely destroyed, and two identical gamma
photons are released. The energy of the two photons has been transformed from the mass of the two
particles.


\subsubsection{Mass Defect}

In a nucleus, the total mass of the nucleus is less than the mass of it's constituent nucleons. This
\textbf{mass defect} causes what is called the \textbf{nuclear binding energy} of a nucleus. This is
the energy that is required to overcome the energy deficit caused by this mass defect, to "break
apart" a nucleus.

The most stable elements have the highest nuclear binding energy per nucleon. 

The nuclear binding energy can be used with \hyperref[Einstein's Energy-Mass Equation]{Einstein's
Energy-Mass Equation}.

\subsubsection{Nuclear Fission}

In nuclear fission, a nucleus with a high mass number is split. The net binding energy of the
original nucleus is less than the net binding energy of all of the particles released, meaning that
energy is released. 

The nucleus with the highest binding energy per nucleon is Iron, so all energy-releasing nuclear
nuclear reactions "travel towards" Iron.

\subsubsection{Induced Nuclear Fission}

In induced nuclear fission, a slow neutron is absorbed by a nucleus, which becomes unstable, and
splits. This releases several fast neutrons, which can be slowed down by a moderator, to be absorbed
by other nuclei.

To control the rate of nuclear fission, control rods are inserted, which absorb some of the
neutrons, reducing the rate of fission. This prevents a nuclear reactor from becoming a nuclear
bomb.

The substance that is being split is usually kept in rods, referred to as "fuel rods".

\subsubsection{Nuclear Fusion}

To fuse two nuclei, you need to bring them extremely close together (within a few femtometers). This
means that the nuclei need to be at an extremely high temperature, as at low temperatures the
particles are not moving fast enough to get close enough to trigger fusion.

Fusion occurs in stars. We have successfully triggered fusion on the earth, but we have not yet been
able to use it to generate energy.

\section{Medical Imaging}

\subsection{Using X-Rays}

\subsubsection{X-Ray tubes}

An X-Ray tube produces x-rays. It consists of an evacuated tube containing two electrodes, with the
cathode being a heated filament. This causes electrons to be emitted by \textbf{thermionic
emission}. The anode is made of a metal, called the target metal. When the electrons hit the target
metal and decelerate, they produce x-ray photons, which are emitted through a window in the tube.

\subsubsection{X-Ray Attenuation Mechanisms}

\textbf{Simple Scatter} - Simple scatter occurs for X-Ray photons with energy in the range
1-20\unit{\kilo \electronvolt}. The X-Ray photon interacts with an electron in the atom, but it's
energy is less than the work function of the electron, so it simply bounces off.

This type of mechanism is not significant in hospital radiography, since they use potential
differences greater than 20\unit{\kilo \volt}.

\textbf{Photoelectric Effect} - For X-Ray photons with energy less than 100\unit{\kilo
\electronvolt} this mechanism is significant. The X-Ray photon is absorbed by one of the electrons
in the atom, causing the electron to escape from the atom. This type of mechanism is dominant in
hospital radiography.

\textbf{Compton Scattering} - For X-ray photons with energy in the range 0.5-5.0\unit{\mega
\electronvolt}. Compton scattering is extremely similar to the photoelectric effect, except the
X-ray photon does not disappear completely - instead being scattered with reduced energy.

\textbf{Pair Production} - This occurs when X-ray photons have energy equal or greater to
1.02\unit{\mega \electronvolt}, where an X-ray photon interacts with an atom's nucleus. The photon
disappears and the electromagnetic energy of the photon is used to create an electron and a
positron.

\subsubsection{Modelling X-Ray Attenuation}

\begin{equation}
	I = I_0 e^{-\mu x}
\end{equation}

Where:

\begin{itemize}
	\item[\(I\)] = Intensity
	\item[\(I_0\)] = Initial Intensity
	\item[\(\mu\)] = Attenuation Coefficient
	\item[\(x\)] = Distance Travelled   
\end{itemize}

\subsubsection{X-Ray Scans}

An X-Ray scan works by firing X-Rays at a patients, and sensing the intensities of the X-Rays that
pass through. Since the denser materials (e.g. bones) have a higher Attenuation Coefficient, the
X-Rays that pass through are less intense than the X-Rays that only go through soft tissue.

The resulting readings can be displayed as an image.

\subsubsection{Contrast Mediums}

Since the absorption coefficient of blood and soft tissue are very similar, it isn't usually
possible to see blood flow on an X-ray scan. To see this, a contrast material is injected into the
patient (either iodine or barium). These compounds have a much higher attenuation coefficient,
meaning that the patient's blood reduces the intensity of resulting X-Rays far more, allowing us to
see the blood flow in a patient's body.

\subsubsection{CAT Scans}

Computerised Axial Tomography Scanning, or CAT Scanning for short, is a method to take a 3
dimensional x-ray scan of a patient. The process works by rotating x-rays and a detector array
around a patient at high speed. For each full rotation, a two-dimensional image or "slice" is
acquired. The X-Ray tube spins in a spiral path along the length of the patient's body.

Each slice can either be viewed individually, or all the slices can be assembled into one three
dimensional image of the patient.

CAT Scans allow doctors to see the shape, size and location of disorders such as tumours. However,
CAT Scans are quite prolonged and expose the patient to a much greater dose of radiation.

\subsection{Diagnostic Methods in Medicine}

\subsubsection{Medical Tracers}

A medical tracer is a radioisotope combined with a compound designed to target a specific area of
the body. Radioisotopes have extremely short half-lives, as the radiation dose the patient
experiences should not last for a long time after the procedure is carried out.

Common medical tracers include Technetium-99m and Fluorine-18. 

\subsubsection{The Gamma Camera}

A gamma camera detects gamma photons emitted from a medical tracer injected into a patient, and an
image is constructed indicating the concentration of the tracer in a patient's body.

The gamma photons travel towards the collimator, a honeycomb of long thin tubes made from lead.
These tubes block any photons arriving at an angle to the axis of the tubes.

The gamma photons then hit a scintillator. A single gamma photon striking the scintillator releases
thousands of photons of visible light. These photons then pass through an array of photomultiplier
tubes, which are arranged in a hexagonal pattern. A photon of light is converted into an electrical
pulse, which is detected by the computer, which can be used to construct a high-quality image
showing the concentrations of the medical tracer within a patient's body.

\subsubsection{PET Scanner}

A PET scanner uses a compound that is similar to glucose, except it has a radioactive fluorine-18
atom. The positrons released by fluorine-18 collide with electrons in the patient, which annihilate
and release two gamma photons travelling in opposite directions.

These gamma rays are then detected by gamma detectors that are placed in a ring around the patient.
This can then be constructed into an image, similarly to how a CAT scan works. However, PET scans
are far more expensive than CAT scans, and take far longer.

PET Scanners only tend to be at larger hospitals.

\subsection{Using Ultrasound}

Ultrasound refers to sound waves with a wavelength greater than 20\unit{\kilo \hertz}. Ultrasound
can be generated by applying a high-frequency alternating p.d. across opposite faces of a crystal.
This repeatedly expands and compresses the crystal, generating ultrasound.

Ultrasound waves are reflected at the boundaries between substances. Since waves take time to
travel, we can read these reflections, and read the different times at which reflected waves were
detected, allowing us to measure the distance to various boundaries in the patient's body.

\subsubsection{A Scans and B Scans}

An A scan is when a single transducer is used to record ultrasound's reflection along a single line
in a patient. 

A B-Scan is when a number of A-Scans are viewed in a 2D image on a screen. This occurs when the
transducer is moved along a patient's screen.

\subsubsection{Acoustic Impedance}

\begin{equation}
	Z = \rho c
\end{equation}

Where:

\begin{itemize}
\item[\(Z\)] = The Acoustic Impedance of a Substance / 
\item[\(\rho\)] = The density of the substance / \unit{\kilogram \per \meter \cubed}
\item[\(c\)] = The speed of ultrasound in that substance / \unit{\meter \per \second} 
\end{itemize}

At a boundary:

\begin{equation}
	\frac{I_r}{I_0} = \left(\frac{Z_2 - Z_1}{Z_2 + Z_1}\right)^2
\end{equation}

Where:

\begin{itemize}
	\item[\(I_r\)] = Reflected Intensity / \unit{\watt \per \meter \squared}
	\item[\(I_0\)] = Incident Intensity / \unit{\watt \per \meter \squared}
	\item[\(Z_1\)] = Impedance of Material 1 / \unit{\kilogram \per \meter \squared \per
	\second}
	\item[\(Z_2\)] = Impedance of Material 2 / \unit{\kilogram \per \meter \squared \per
	\second}
\end{itemize}

Note that the ratio \(\frac{I_r}{I_0} \) is known as the intensity reflection coefficient.

\subsubsection{Coupling Gel}

In ultrasound scans, there will always be a small pocket of air between the transducer and the
patient's skin. This air pocket is filled with a coupling gel, which has an acoustic impedance
similar to that of the skin, meaning that negligible reflection occurs at the boundary between the
two substances.

The term acoustic matching refers to when two substances are used which have similar values of
acoustic impedance.

\subsubsection{Doppler Imaging}

The frequency of ultrasound changes when it is reflected off a moving object. This allows us to
measure the velocity of blood flow through major arteries, as we can measure the change in
wavelength of the ultrasound as it returns to the transducer.

The change in frequency can be calculated using the following formula:

\begin{equation}
	\Delta f = \frac{2fv\cos \theta}{c}
\end{equation}

Which can be rearranged to calculate the velocity of the blood.