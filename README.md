# A Level Notes

My notes for my A-level subjects, to be sat in the 2023 exam cycle.

## Subjects

- Further Mathematics - Core Pure 1 & 2, Further Mechanics 1 & 2
- Computer Science - AQA
- Physics - OCR

## License

MIT License, see License file.

## Usage

To get a built PDF of the full set of notes, press the download button on GitLab and download the
output of the build task.